/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : microservice-exam

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 12/09/2019 17:58:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for exam_answer
-- ----------------------------
DROP TABLE IF EXISTS `exam_answer`;
CREATE TABLE `exam_answer`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `exam_record_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试记录id',
  `subject_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `type` int(11) NULL DEFAULT NULL,
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '答案',
  `answer_type` int(11) NULL DEFAULT NULL COMMENT '答题类型，0：正确，1：错误',
  `score` int(11) NULL DEFAULT NULL COMMENT '实际得分',
  `mark_status` int(255) NULL DEFAULT NULL,
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答题表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_answer
-- ----------------------------
INSERT INTO `exam_answer` VALUES ('621014937229201408', '621014936574889984', '590972141118296064', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:11:39', 'admin', '2019-09-10 16:14:33', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015015717212160', '621014936574889984', '590972250212143104', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:11:58', 'admin', '2019-09-10 16:14:39', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015046385963008', '621014936574889984', '590972363110223872', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:12:05', 'admin', '2019-09-10 16:14:41', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015103323639808', '621014936574889984', '590972473877598208', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:12:19', 'admin', '2019-09-10 16:14:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015165600665600', '621014936574889984', '590972590177259520', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:12:33', 'admin', '2019-09-10 16:14:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015245317607424', '621014936574889984', '590972723384160256', 0, 'D', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:12:52', 'admin', '2019-09-10 16:14:51', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015269992697856', '621014936574889984', '590972834734542848', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:12:58', 'admin', '2019-09-10 16:14:53', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015297805127680', '621014936574889984', '590972929693585408', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:13:05', 'admin', '2019-09-10 16:14:57', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015318550155264', '621014936574889984', '590973027995488256', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:13:10', 'admin', '2019-09-10 16:14:59', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015338477293568', '621014936574889984', '590973115157319680', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:13:15', 'admin', '2019-09-10 16:15:02', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015364674916352', '621014936574889984', '590973253544185856', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-10 16:13:21', 'admin', '2019-09-10 16:15:05', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621015406345326592', '621014936574889984', '590973360662515712', 0, 'B', 0, 5, 1, 'huiyuanjian', '2019-09-10 16:13:31', 'admin', '2019-09-10 16:15:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621028755955126273', '621028755892211712', '590970743878193152', 0, 'D', NULL, NULL, 0, 'huiyuanjian', '2019-09-10 17:06:34', 'huiyuanjian', '2019-09-10 17:06:47', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621028841950941184', '621028755892211712', '590970887432441856', 0, 'D', NULL, NULL, 0, 'huiyuanjian', '2019-09-10 17:06:54', 'huiyuanjian', '2019-09-10 17:06:54', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621028882568581120', '621028755892211712', '590971026880466944', 0, 'B', NULL, NULL, 0, 'huiyuanjian', '2019-09-10 17:07:04', 'huiyuanjian', '2019-09-10 17:07:04', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621028903842091008', '621028755892211712', '590971151128334336', 0, 'B', NULL, NULL, 0, 'huiyuanjian', '2019-09-10 17:07:09', 'huiyuanjian', '2019-09-10 17:07:09', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621028929309904896', '621028755892211712', '590971273539096576', 0, 'D', NULL, NULL, 0, 'huiyuanjian', '2019-09-10 17:07:15', 'huiyuanjian', '2019-09-10 17:07:15', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029124932243457', '621029124894494720', '590970743878193152', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-10 17:08:02', 'huiyuanjian', '2019-09-10 17:08:02', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029177583341569', '621029177541398528', '590970743878193152', 0, 'D', 0, 5, 1, 'huiyuanjian', '2019-09-10 17:08:14', 'huiyuanjian', '2019-09-10 17:08:23', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029224928645120', '621029177541398528', '590970887432441856', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-10 17:08:25', 'huiyuanjian', '2019-09-10 17:08:25', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029236974686208', '621029177541398528', '590971026880466944', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-10 17:08:28', 'huiyuanjian', '2019-09-10 17:08:28', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029249054281728', '621029177541398528', '590971151128334336', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-10 17:08:31', 'huiyuanjian', '2019-09-10 17:08:31', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029284995272704', '621029177541398528', '590971273539096576', 0, 'D', 0, 5, 1, 'huiyuanjian', '2019-09-10 17:08:40', 'huiyuanjian', '2019-09-10 17:08:40', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029309984935936', '621029177541398528', '590971402639773696', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-10 17:08:46', 'huiyuanjian', '2019-09-10 17:08:46', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029372295516160', '621029177541398528', '590971508109742080', 0, 'C', 0, 5, 1, 'huiyuanjian', '2019-09-10 17:09:01', 'huiyuanjian', '2019-09-10 17:09:01', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029394646962176', '621029177541398528', '590971613944614912', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-10 17:09:06', 'huiyuanjian', '2019-09-10 17:09:06', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029423801569280', '621029177541398528', '590971750146248704', 0, 'D', 1, 0, 1, 'huiyuanjian', '2019-09-10 17:09:13', 'huiyuanjian', '2019-09-10 17:09:13', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621029459390238720', '621029177541398528', '590971860041207808', 0, 'C', 0, 5, 1, 'huiyuanjian', '2019-09-10 17:09:21', 'huiyuanjian', '2019-09-10 17:09:21', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621287859462213632', '621287859273469952', '621286984803028992', 0, 'A', 0, 50, 1, 'huiyuanjian', '2019-09-11 10:16:09', 'huiyuanjian', '2019-09-11 10:16:17', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288066098794496', '621287859273469952', '621287044316008448', 0, 'D', 0, 50, 1, 'huiyuanjian', '2019-09-11 10:16:58', 'huiyuanjian', '2019-09-11 10:16:58', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288193416892417', '621288193379143680', '621286984803028992', 0, 'D', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:17:28', 'huiyuanjian', '2019-09-11 10:18:01', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288353458950144', '621288193379143680', '621287044316008448', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:18:06', 'huiyuanjian', '2019-09-11 10:18:06', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288580303687681', '621288580257550336', '590972141118296064', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:01', 'admin', '2019-09-11 10:27:31', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288643163721728', '621288580257550336', '590972250212143104', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:16', 'huiyuanjian', '2019-09-11 10:19:16', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288650109489152', '621288580257550336', '590972363110223872', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:17', 'huiyuanjian', '2019-09-11 10:19:17', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288658158358528', '621288580257550336', '590972473877598208', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:19', 'huiyuanjian', '2019-09-11 10:19:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288664735027200', '621288580257550336', '590972590177259520', 0, 'A', 0, 5, 1, 'huiyuanjian', '2019-09-11 10:19:21', 'huiyuanjian', '2019-09-11 10:19:21', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288673203326976', '621288580257550336', '590972723384160256', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:23', 'huiyuanjian', '2019-09-11 10:19:23', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288686839009280', '621288580257550336', '590972834734542848', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:26', 'huiyuanjian', '2019-09-11 10:19:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288696611737600', '621288580257550336', '590972929693585408', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:28', 'huiyuanjian', '2019-09-11 10:19:28', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288705713377280', '621288580257550336', '590973027995488256', 0, 'A', 0, 5, 1, 'huiyuanjian', '2019-09-11 10:19:30', 'huiyuanjian', '2019-09-11 10:19:30', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288718422118400', '621288580257550336', '590973115157319680', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:33', 'huiyuanjian', '2019-09-11 10:19:33', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288734364667904', '621288580257550336', '590973253544185856', 0, 'A', 0, 5, 1, 'huiyuanjian', '2019-09-11 10:19:37', 'huiyuanjian', '2019-09-11 10:19:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621288753876570112', '621288580257550336', '590973360662515712', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 10:19:42', 'huiyuanjian', '2019-09-11 10:19:42', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621294281981825024', '621294281923104768', '621286984803028992', 0, 'A', 0, 50, 1, 'huiyuanjian', '2019-09-11 10:41:40', 'huiyuanjian', '2019-09-11 10:41:53', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621294408612057088', '621294281923104768', '621287044316008448', 0, 'D', 0, 50, 1, 'huiyuanjian', '2019-09-11 10:42:10', 'huiyuanjian', '2019-09-11 10:42:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621294558420013057', '621294558352904192', '621286984803028992', 0, 'A', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 10:42:46', 'huiyuanjian', '2019-09-11 10:47:13', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621295675237339136', '621294558352904192', '621287044316008448', 0, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 10:47:12', 'huiyuanjian', '2019-09-11 10:47:15', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621299568008630273', '621299567954104320', '621286984803028992', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-11 11:02:40', 'huiyuanjian', '2019-09-11 11:05:56', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621300076668653568', '621299567954104320', '621287044316008448', 0, 'D', 0, 50, 1, 'huiyuanjian', '2019-09-11 11:04:41', 'huiyuanjian', '2019-09-11 11:06:07', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621301280085774337', '621301280052219904', '621286984803028992', 0, 'A', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 11:09:28', 'huiyuanjian', '2019-09-11 11:10:00', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621301419286335488', '621301280052219904', '621287044316008448', 0, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 11:10:02', 'huiyuanjian', '2019-09-11 11:11:02', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621301450391293952', '621301280052219904', '621300871069831168', 0, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 11:10:09', 'huiyuanjian', '2019-09-11 11:11:03', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621301988512108545', '621301988478554112', '621286984803028992', 0, 'A', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 11:12:17', 'huiyuanjian', '2019-09-11 11:12:35', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621302097308160000', '621301988478554112', '621287044316008448', 0, 'D', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 11:12:43', 'huiyuanjian', '2019-09-11 11:13:16', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621302225142157312', '621301988478554112', '621300871069831168', 0, '', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 11:13:14', 'huiyuanjian', '2019-09-11 11:13:14', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621302320294137857', '621302320256389120', '621286984803028992', 0, 'A', 0, 50, 1, 'huiyuanjian', '2019-09-11 11:13:36', 'huiyuanjian', '2019-09-11 11:13:41', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621302466079756288', '621302320256389120', '621287044316008448', 0, 'D', 0, 40, 1, 'huiyuanjian', '2019-09-11 11:14:11', 'huiyuanjian', '2019-09-11 11:14:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621302526456762368', '621302320256389120', '621300871069831168', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-11 11:14:26', 'huiyuanjian', '2019-09-11 11:14:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621303592338460673', '621303592309100544', '621286984803028992', 0, 'A', 0, 50, 1, 'huiyuanjian', '2019-09-11 11:18:40', 'huiyuanjian', '2019-09-11 11:19:35', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621303893585956864', '621303592309100544', '621287044316008448', 0, 'D', 0, 40, 1, 'huiyuanjian', '2019-09-11 11:19:51', 'huiyuanjian', '2019-09-11 11:19:51', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621304150856175616', '621303592309100544', '621300871069831168', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-11 11:20:53', 'huiyuanjian', '2019-09-11 11:20:53', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621341403930300417', '621341403858997248', '621286984803028992', 0, '', 1, 0, 1, 'huiyuanjian', '2019-09-11 13:48:55', 'huiyuanjian', '2019-09-11 13:48:59', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621341481965326336', '621341403858997248', '621287044316008448', 0, NULL, 1, 0, 1, 'huiyuanjian', '2019-09-11 13:49:13', 'huiyuanjian', '2019-09-11 13:57:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621343421096923136', '621341403858997248', '621300871069831168', 0, '', 1, 0, 1, 'huiyuanjian', '2019-09-11 13:56:56', 'huiyuanjian', '2019-09-11 13:57:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621344360872677377', '621344360818151424', '621286984803028992', 0, 'A', 0, 50, 1, 'huiyuanjian', '2019-09-11 14:00:40', 'huiyuanjian', '2019-09-11 14:01:03', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621344507962724352', '621344360818151424', '621287044316008448', 0, 'D', 0, 40, 1, 'huiyuanjian', '2019-09-11 14:01:15', 'huiyuanjian', '2019-09-11 14:01:15', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621344525339725824', '621344360818151424', '621300871069831168', 0, 'A', 1, 0, 1, 'huiyuanjian', '2019-09-11 14:01:19', 'huiyuanjian', '2019-09-11 14:01:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621345450166980609', '621345450120843264', '621286984803028992', 0, 'B', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:04:59', 'huiyuanjian', '2019-09-11 14:05:14', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621345547437084672', '621345450120843264', '621287044316008448', 0, '', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:05:23', 'huiyuanjian', '2019-09-11 14:10:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621345561529946112', '621345450120843264', '621300871069831168', 0, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:05:26', 'huiyuanjian', '2019-09-11 14:05:50', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621346214545330176', '621345450120843264', '621346123818340352', 0, '', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:08:02', 'huiyuanjian', '2019-09-11 14:10:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621348109112446977', '621348109066309632', '621347666936336384', 0, 'A', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:15:33', 'huiyuanjian', '2019-09-11 14:24:34', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621348174304514048', '621348109066309632', '621347797207224320', 0, 'D', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:15:49', 'huiyuanjian', '2019-09-11 14:24:53', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621348201613627392', '621348109066309632', '621348039700910080', 0, '', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:15:55', 'huiyuanjian', '2019-09-11 14:25:01', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621350823867322369', '621350823825379328', '621347666936336384', 0, 'A', 0, 5, 1, 'huiyuanjian', '2019-09-11 14:26:21', 'huiyuanjian', '2019-09-11 14:26:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621351523267514368', '621350823825379328', '621347797207224320', 0, 'D', 0, 5, 1, 'huiyuanjian', '2019-09-11 14:29:07', 'huiyuanjian', '2019-09-11 14:29:07', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621354744417488896', '621350823825379328', '621348039700910080', 0, NULL, 1, 0, 1, 'huiyuanjian', '2019-09-11 14:41:55', 'huiyuanjian', '2019-09-11 14:41:55', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621375734337900545', '621375734077853696', '621347666936336384', 0, 'A', 0, 5, 1, 'huiyuanjian', '2019-09-11 16:05:20', 'anonymousUser', '2019-09-12 15:39:50', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621378176580456448', '621375734077853696', '621347797207224320', 0, 'D', 0, 5, 1, 'huiyuanjian', '2019-09-11 16:15:02', 'huiyuanjian', '2019-09-11 16:15:02', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621378209467994112', '621375734077853696', '621348039700910080', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-11 16:15:10', 'huiyuanjian', '2019-09-11 16:15:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621407598226313217', '621407598066929664', '621347666936336384', 0, 'A', 0, 5, 1, 'huiyuanjian', '2019-09-11 18:11:57', 'huiyuanjian', '2019-09-11 18:12:20', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621407637820542976', '621407598066929664', '621347797207224320', 0, 'D', 0, 5, 1, 'huiyuanjian', '2019-09-11 18:12:06', 'huiyuanjian', '2019-09-11 18:12:21', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621407674394873856', '621407598066929664', '621348039700910080', 0, 'D', 0, 5, 1, 'huiyuanjian', '2019-09-11 18:12:15', 'huiyuanjian', '2019-09-11 18:12:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621408677760471041', '621408677722722304', '621347666936336384', 0, '', NULL, NULL, 0, 'huiyuanjian', '2019-09-11 18:16:14', 'huiyuanjian', '2019-09-11 18:20:41', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621414089884831745', '1111', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-11 18:37:44', 'anonymousUser', '2019-09-11 18:37:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621414204624211969', '621414204515160064', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-11 18:38:12', 'anonymousUser', '2019-09-11 18:38:12', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621695632943484928', '621695631689388032', '621347666936336384', 0, 'A', 0, 5, 1, 'huiyuanjian', '2019-09-12 13:16:29', 'admin', '2019-09-12 13:21:41', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621695754054012928', '621695631689388032', '621347797207224320', 0, 'D', 0, 5, 1, 'huiyuanjian', '2019-09-12 13:16:58', 'admin', '2019-09-12 13:21:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621695782789189632', '621695631689388032', '621348039700910080', 0, 'D', 0, 5, 1, 'huiyuanjian', '2019-09-12 13:17:05', 'admin', '2019-09-12 13:21:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621702311437275137', '621702311114313728', '621347666936336384', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-12 13:43:02', 'huiyuanjian', '2019-09-12 14:15:36', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621702545441689600', '621702311114313728', '621347797207224320', 0, 'B', 1, 0, 1, 'huiyuanjian', '2019-09-12 13:43:58', 'huiyuanjian', '2019-09-12 14:15:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621702623527047168', '621702311114313728', '621348039700910080', 0, 'C', 1, 0, 1, 'huiyuanjian', '2019-09-12 13:44:16', 'huiyuanjian', '2019-09-12 14:13:24', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621732385570033664', '621732385481953280', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 15:42:32', 'anonymousUser', '2019-09-12 15:42:32', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621733035280306177', '621733035221585920', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 15:45:07', 'anonymousUser', '2019-09-12 15:45:07', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621733104628928513', '621733104566013952', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 15:45:23', 'anonymousUser', '2019-09-12 15:45:23', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621735100865974273', '621735100765310976', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 15:53:19', 'anonymousUser', '2019-09-12 15:53:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621736813211553792', '621736812783734784', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 16:00:08', 'anonymousUser', '2019-09-12 16:00:08', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621736871311052800', '621736871185223680', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 16:00:21', 'anonymousUser', '2019-09-12 16:00:21', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621736902403428353', '621736902277599232', '621347666936336384', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 16:00:29', 'anonymousUser', '2019-09-12 16:00:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621743428161638401', '621743427385692160', '621347666936336384', 0, 'A', 0, 5, 1, 'anonymousUser', '2019-09-12 16:26:25', 'anonymousUser', '2019-09-12 16:30:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621744704672894976', '621743427385692160', '621347797207224320', 0, 'B', 1, 0, 1, 'anonymousUser', '2019-09-12 16:31:29', 'anonymousUser', '2019-09-12 16:31:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_answer` VALUES ('621744914966908928', '621743427385692160', '621348039700910080', 0, 'D', 0, 5, 1, 'anonymousUser', '2019-09-12 16:32:19', 'anonymousUser', '2019-09-12 16:32:19', 0, 'EXAM', 'gitee');

-- ----------------------------
-- Table structure for exam_course
-- ----------------------------
DROP TABLE IF EXISTS `exam_course`;
CREATE TABLE `exam_course`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `course_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `college` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学院',
  `major` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业',
  `teacher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '老师',
  `course_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程描述',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课程表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_course
-- ----------------------------
INSERT INTO `exam_course` VALUES ('590968789617741824', '计算机基础', '信息学院', '软件工程', '', '计算机基础', 'admin', '2019-06-19 18:18:59', 'admin', '2019-06-23 13:55:51', 0, 'EXAM', 'gitee');
INSERT INTO `exam_course` VALUES ('590968881187786752', '大学语文', '信息学院', '软件工程', '陈老师', '大学语文', 'admin', '2019-06-19 18:19:20', 'admin', '2019-07-19 22:09:42', 0, 'EXAM', 'gitee');

-- ----------------------------
-- Table structure for exam_examination
-- ----------------------------
DROP TABLE IF EXISTS `exam_examination`;
CREATE TABLE `exam_examination`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `examination_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试名称',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试类型',
  `attention` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试注意事项',
  `start_time` timestamp(0) NULL DEFAULT NULL COMMENT '考试开始时间',
  `end_time` timestamp(0) NULL DEFAULT NULL COMMENT '考试结束时间',
  `total_score` int(11) NULL DEFAULT NULL COMMENT '总分',
  `status` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试状态',
  `avatar_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片ID',
  `course_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '考试信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_examination
-- ----------------------------
INSERT INTO `exam_examination` VALUES ('590969316204220416', '全国计算机统考练习题10道', '0', '练习', '2019-09-10 00:00:00', '2019-09-11 00:00:00', 50, '0', NULL, '590968789617741824', '全国计算机统考练习题10道', 'admin', '2019-06-19 18:21:04', 'admin', '2019-09-10 17:06:04', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination` VALUES ('590969514372501504', '四川省2016年普通高考文科综合能力测试-语文部分', '0', '注意事项:\n1.本试卷分第工卷(选择题)和第II卷(非选择题)两部分。答卷前，考生务必将白己的姓名、准考证号填写在答题卡上。\n2.回答第Ⅰ卷时，选出每小题答案后，用铅笔把答题卡上对应题目的答案标号涂黑。如需改动，用橡皮擦干净后，再选涂其它答案标号。写在本试卷上无效。\n3.回答第Ⅱ卷时，将答案写在答题卡上。写在本试卷上无效。\n4.考试结束后，将本试卷和答题卡一并交回。', '2019-09-10 00:00:00', '2019-09-11 16:02:00', 60, '0', NULL, '590968881187786752', '四川省2016年普通高考-文科综合能力测试', 'admin', '2019-06-19 18:21:51', 'admin', '2019-09-10 16:11:21', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination` VALUES ('621286854880268288', 'AWS认证测试', '0', 'AWS认证测试 AWS认证测试 AWS认证测试', '2019-09-11 11:00:00', '2019-09-11 16:00:00', 100, '0', NULL, '590968789617741824', 'AWS认证测试 AWS认证测试 AWS认证测试', 'admin', '2019-09-11 10:12:09', 'admin', '2019-09-11 13:48:06', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination` VALUES ('621347429198991360', 'AWS认证考试', '0', 'AWS认证考试', '2019-09-11 14:00:00', '2019-09-12 17:00:00', 100, '0', NULL, '590968789617741824', 'AWS认证考试', 'admin', '2019-09-11 14:12:51', 'admin', '2019-09-11 18:11:48', 0, 'EXAM', 'gitee');

-- ----------------------------
-- Table structure for exam_examination_record
-- ----------------------------
DROP TABLE IF EXISTS `exam_examination_record`;
CREATE TABLE `exam_examination_record`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `examination_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试ID',
  `start_time` timestamp(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` timestamp(0) NULL DEFAULT NULL COMMENT '结束时间',
  `score` int(11) NULL DEFAULT NULL COMMENT '成绩',
  `correct_number` int(11) NULL DEFAULT NULL COMMENT '正确题目数量',
  `incorrect_number` int(11) NULL DEFAULT NULL COMMENT '错误题目数量',
  `_status` int(11) NULL DEFAULT NULL,
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '考试记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_examination_record
-- ----------------------------
INSERT INTO `exam_examination_record` VALUES ('1111', '621013765118365696', '621347429198991360', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'anonymousUser', '2019-09-11 18:37:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621014936574889984', '621013765118365696', '590969514372501504', '2019-09-10 16:11:39', '2019-09-10 16:13:31', 5, 1, 11, 3, 'huiyuanjian', '2019-09-10 16:11:39', 'huiyuanjian', '2019-09-10 16:13:31', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621028755892211712', '621013765118365696', '590969316204220416', '2019-09-10 17:06:34', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-10 17:06:34', 'huiyuanjian', '2019-09-10 17:06:34', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621029124894494720', '621013765118365696', '590969316204220416', '2019-09-10 17:08:02', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-10 17:08:02', 'huiyuanjian', '2019-09-10 17:08:02', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621029177541398528', '621013765118365696', '590969316204220416', '2019-09-10 17:08:14', '2019-09-10 17:09:21', 20, 4, 6, 3, 'huiyuanjian', '2019-09-10 17:08:14', 'huiyuanjian', '2019-09-10 17:09:21', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621287859273469952', '621013765118365696', '621286854880268288', '2019-09-11 10:16:09', '2019-09-11 10:16:58', 100, 2, 0, 3, 'huiyuanjian', '2019-09-11 10:16:09', 'huiyuanjian', '2019-09-11 10:16:58', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621288193379143680', '621013765118365696', '621286854880268288', '2019-09-11 10:17:28', '2019-09-11 10:18:07', 0, 0, 2, 3, 'huiyuanjian', '2019-09-11 10:17:28', 'huiyuanjian', '2019-09-11 10:18:07', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621288580257550336', '621013765118365696', '590969514372501504', '2019-09-11 10:19:01', '2019-09-11 10:19:42', 15, 3, 9, 3, 'huiyuanjian', '2019-09-11 10:19:01', 'huiyuanjian', '2019-09-11 10:19:42', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621294281923104768', '621013765118365696', '621286854880268288', '2019-09-11 10:41:40', '2019-09-11 10:42:10', 100, 2, 0, 3, 'huiyuanjian', '2019-09-11 10:41:40', 'huiyuanjian', '2019-09-11 10:42:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621294558352904192', '621013765118365696', '621286854880268288', '2019-09-11 10:42:46', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 10:42:46', 'huiyuanjian', '2019-09-11 10:42:46', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621299567954104320', '621013765118365696', '621286854880268288', '2019-09-11 11:02:40', '2019-09-11 11:06:07', 50, 1, 1, 3, 'huiyuanjian', '2019-09-11 11:02:40', 'huiyuanjian', '2019-09-11 11:06:07', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621301280052219904', '621013765118365696', '621286854880268288', '2019-09-11 11:09:28', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 11:09:28', 'huiyuanjian', '2019-09-11 11:09:28', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621301988478554112', '621013765118365696', '621286854880268288', '2019-09-11 11:12:17', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 11:12:17', 'huiyuanjian', '2019-09-11 11:12:17', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621302320256389120', '621013765118365696', '621286854880268288', '2019-09-11 11:13:36', '2019-09-11 11:14:26', 90, 2, 1, 3, 'huiyuanjian', '2019-09-11 11:13:36', 'huiyuanjian', '2019-09-11 11:14:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621303592309100544', '621013765118365696', '621286854880268288', '2019-09-11 11:18:40', '2019-09-11 11:20:53', 90, 2, 1, 3, 'huiyuanjian', '2019-09-11 11:18:40', 'huiyuanjian', '2019-09-11 11:20:53', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621341403858997248', '621013765118365696', '621286854880268288', '2019-09-11 13:48:55', '2019-09-11 13:57:48', 0, 0, 3, 3, 'huiyuanjian', '2019-09-11 13:48:55', 'huiyuanjian', '2019-09-11 13:57:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621344360818151424', '621013765118365696', '621286854880268288', '2019-09-11 14:00:40', '2019-09-11 14:01:19', 90, 2, 1, 3, 'huiyuanjian', '2019-09-11 14:00:40', 'huiyuanjian', '2019-09-11 14:01:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621345450120843264', '621013765118365696', '621286854880268288', '2019-09-11 14:04:59', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:04:59', 'huiyuanjian', '2019-09-11 14:04:59', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621348109066309632', '621013765118365696', '621347429198991360', '2019-09-11 14:15:33', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 14:15:33', 'huiyuanjian', '2019-09-11 14:15:33', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621350823825379328', '621013765118365696', '621347429198991360', '2019-09-11 14:26:21', '2019-09-11 14:41:55', 10, 2, 1, 3, 'huiyuanjian', '2019-09-11 14:26:21', 'huiyuanjian', '2019-09-11 14:41:55', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621375734077853696', '621013765118365696', '621347429198991360', '2019-09-11 16:05:20', '2019-09-11 16:15:10', 10, 2, 1, 3, 'huiyuanjian', '2019-09-11 16:05:20', 'huiyuanjian', '2019-09-11 16:15:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621407598066929664', '621013765118365696', '621347429198991360', '2019-09-11 18:11:57', '2019-09-11 18:12:27', 15, 3, 0, 3, 'huiyuanjian', '2019-09-11 18:11:57', 'huiyuanjian', '2019-09-11 18:12:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621408677722722304', '621013765118365696', '621347429198991360', '2019-09-11 18:16:14', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-11 18:16:14', 'huiyuanjian', '2019-09-11 18:16:14', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621414204515160064', '621013765118365696', '621347429198991360', '2019-09-11 18:38:12', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-11 18:38:12', 'anonymousUser', '2019-09-11 18:38:12', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621695631689388032', '621013765118365696', '621347429198991360', '2019-09-12 13:16:29', '2019-09-12 13:17:06', 15, 3, 0, 3, 'huiyuanjian', '2019-09-12 13:16:29', 'huiyuanjian', '2019-09-12 13:17:06', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621702311114313728', '621013765118365696', '621347429198991360', '2019-09-12 13:43:02', '2019-09-12 14:19:41', 0, 0, 3, 3, 'huiyuanjian', '2019-09-12 13:43:02', 'anonymousUser', '2019-09-12 14:19:41', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621732385481953280', '621013765118365696', '621347429198991360', '2019-09-12 15:42:32', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 15:42:32', 'anonymousUser', '2019-09-12 15:42:32', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621733035221585920', '621013765118365696', '621347429198991360', '2019-09-12 15:45:07', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 15:45:07', 'anonymousUser', '2019-09-12 15:45:07', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621733104566013952', '621013765118365696', '621347429198991360', '2019-09-12 15:45:23', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 15:45:23', 'anonymousUser', '2019-09-12 15:45:23', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621735100765310976', '621013765118365696', '621347429198991360', '2019-09-12 15:53:19', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 15:53:19', 'anonymousUser', '2019-09-12 15:53:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621736812783734784', '621013765118365696', '621347429198991360', '2019-09-12 16:00:07', NULL, NULL, NULL, NULL, 0, 'huiyuanjian', '2019-09-12 16:00:07', 'huiyuanjian', '2019-09-12 16:00:07', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621736871185223680', '621013765118365696', '621347429198991360', '2019-09-12 16:00:21', NULL, NULL, NULL, NULL, 0, 'anonymousUser', '2019-09-12 16:00:21', 'anonymousUser', '2019-09-12 16:00:21', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621736902277599232', '621013765118365696', '621347429198991360', '2019-09-12 16:00:29', '2019-09-12 16:06:11', NULL, NULL, NULL, 2, 'huiyuanjian', '2019-09-12 16:00:29', 'huiyuanjian', '2019-09-12 16:00:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_examination_record` VALUES ('621743427385692160', '621013765118365696', '621347429198991360', '2019-09-12 16:26:25', '2019-09-12 16:33:16', 10, 2, 1, 3, 'huiyuanjian', '2019-09-12 16:26:25', 'huiyuanjian', '2019-09-12 16:33:16', 0, 'EXAM', 'gitee');

-- ----------------------------
-- Table structure for exam_examination_subject
-- ----------------------------
DROP TABLE IF EXISTS `exam_examination_subject`;
CREATE TABLE `exam_examination_subject`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `examination_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '考试ID',
  `subject_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型 0-选择题，1-简答题',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `serial_number` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '考试题目表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_examination_subject
-- ----------------------------
INSERT INTO `exam_examination_subject` VALUES ('590970743886581760', '590969316204220416', '590970743878193152', 0, 'EXAM', 'gitee', 1);
INSERT INTO `exam_examination_subject` VALUES ('590970887440830464', '590969316204220416', '590970887432441856', 0, 'EXAM', 'gitee', 2);
INSERT INTO `exam_examination_subject` VALUES ('590971026888855552', '590969316204220416', '590971026880466944', 0, 'EXAM', 'gitee', 3);
INSERT INTO `exam_examination_subject` VALUES ('590971151145111552', '590969316204220416', '590971151128334336', 0, 'EXAM', 'gitee', 4);
INSERT INTO `exam_examination_subject` VALUES ('590971273547485184', '590969316204220416', '590971273539096576', 0, 'EXAM', 'gitee', 5);
INSERT INTO `exam_examination_subject` VALUES ('590971402648162304', '590969316204220416', '590971402639773696', 0, 'EXAM', 'gitee', 6);
INSERT INTO `exam_examination_subject` VALUES ('590971508113936384', '590969316204220416', '590971508109742080', 0, 'EXAM', 'gitee', 7);
INSERT INTO `exam_examination_subject` VALUES ('590971613944614913', '590969316204220416', '590971613944614912', 0, 'EXAM', 'gitee', 8);
INSERT INTO `exam_examination_subject` VALUES ('590971750154637312', '590969316204220416', '590971750146248704', 0, 'EXAM', 'gitee', 9);
INSERT INTO `exam_examination_subject` VALUES ('590971860049596416', '590969316204220416', '590971860041207808', 0, 'EXAM', 'gitee', 10);
INSERT INTO `exam_examination_subject` VALUES ('590972141151850496', '590969514372501504', '590972141118296064', 0, 'EXAM', 'gitee', 1);
INSERT INTO `exam_examination_subject` VALUES ('590972250216337408', '590969514372501504', '590972250212143104', 0, 'EXAM', 'gitee', 2);
INSERT INTO `exam_examination_subject` VALUES ('590972363114418176', '590969514372501504', '590972363110223872', 0, 'EXAM', 'gitee', 3);
INSERT INTO `exam_examination_subject` VALUES ('590972473890181120', '590969514372501504', '590972473877598208', 0, 'EXAM', 'gitee', 4);
INSERT INTO `exam_examination_subject` VALUES ('590972590185648128', '590969514372501504', '590972590177259520', 0, 'EXAM', 'gitee', 5);
INSERT INTO `exam_examination_subject` VALUES ('590972723384160257', '590969514372501504', '590972723384160256', 0, 'EXAM', 'gitee', 6);
INSERT INTO `exam_examination_subject` VALUES ('590972834742931456', '590969514372501504', '590972834734542848', 0, 'EXAM', 'gitee', 7);
INSERT INTO `exam_examination_subject` VALUES ('590972929701974016', '590969514372501504', '590972929693585408', 0, 'EXAM', 'gitee', 8);
INSERT INTO `exam_examination_subject` VALUES ('590973027999682560', '590969514372501504', '590973027995488256', 0, 'EXAM', 'gitee', 9);
INSERT INTO `exam_examination_subject` VALUES ('590973115165708288', '590969514372501504', '590973115157319680', 0, 'EXAM', 'gitee', 10);
INSERT INTO `exam_examination_subject` VALUES ('590973253548380160', '590969514372501504', '590973253544185856', 0, 'EXAM', 'gitee', 11);
INSERT INTO `exam_examination_subject` VALUES ('590973360670904320', '590969514372501504', '590973360662515712', 0, 'EXAM', 'gitee', 12);
INSERT INTO `exam_examination_subject` VALUES ('621287044320202752', '621286854880268288', '621287044316008448', 0, 'EXAM', 'gitee', 2);
INSERT INTO `exam_examination_subject` VALUES ('621300871069831169', '621286854880268288', '621300871069831168', 0, 'EXAM', 'gitee', 1);
INSERT INTO `exam_examination_subject` VALUES ('621346123818340353', '621286854880268288', '621346123818340352', 0, 'EXAM', 'gitee', 1);
INSERT INTO `exam_examination_subject` VALUES ('621347666944724992', '621347429198991360', '621347666936336384', 0, 'EXAM', 'gitee', 1);
INSERT INTO `exam_examination_subject` VALUES ('621347797207224321', '621347429198991360', '621347797207224320', 0, 'EXAM', 'gitee', 2);
INSERT INTO `exam_examination_subject` VALUES ('621348039709298688', '621347429198991360', '621348039700910080', 0, 'EXAM', 'gitee', 3);

-- ----------------------------
-- Table structure for exam_knowledge
-- ----------------------------
DROP TABLE IF EXISTS `exam_knowledge`;
CREATE TABLE `exam_knowledge`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `knowledge_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '知识名称',
  `knowledge_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '知识描述',
  `attachment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件ID',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '知识表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_knowledge
-- ----------------------------
INSERT INTO `exam_knowledge` VALUES ('590978901526843392', '四川省2016年普通高考适应性测试', '四川省2016年普通高考适应性测试', '590978944174526464', 0, 'admin', '2019-06-19 18:59:09', 'admin', '2019-07-04 13:51:27', 0, 'EXAM', 'gitee');

-- ----------------------------
-- Table structure for exam_pictures
-- ----------------------------
DROP TABLE IF EXISTS `exam_pictures`;
CREATE TABLE `exam_pictures`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `picture_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '知识名称',
  `attachment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件ID',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for exam_subject_category
-- ----------------------------
DROP TABLE IF EXISTS `exam_subject_category`;
CREATE TABLE `exam_subject_category`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `category_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类描述',
  `parent_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父分类ID',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型: 0-私共,1-公有',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '题目分类表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_subject_category
-- ----------------------------
INSERT INTO `exam_subject_category` VALUES ('590975991732637696', '计算机', NULL, '-1', 30, NULL, 'admin', '2019-06-19 18:47:36', 'admin', '2019-06-19 18:47:36', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_category` VALUES ('590976037467328512', '数据库基础', NULL, '590975991732637696', 30, NULL, 'admin', '2019-06-19 18:47:47', 'admin', '2019-07-04 13:51:03', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_category` VALUES ('590976094983819264', 'Java程序设计', NULL, '590975991732637696', 31, NULL, 'admin', '2019-06-19 18:48:00', 'admin', '2019-06-19 18:48:00', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_category` VALUES ('590976142211682304', '数据结构', NULL, '590975991732637696', 33, NULL, 'admin', '2019-06-19 18:48:12', 'admin', '2019-06-19 18:48:12', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_category` VALUES ('590976191398285312', '地理', NULL, '-1', 31, NULL, 'admin', '2019-06-19 18:48:23', 'admin', '2019-06-19 18:48:23', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_category` VALUES ('590976335996915712', '计算机基础', NULL, '590975991732637696', 34, NULL, 'admin', '2019-06-19 18:48:58', 'admin', '2019-06-19 18:48:58', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_category` VALUES ('602231546270846976', '多选测试', NULL, '-1', 30, NULL, 'admin', '2019-07-20 20:13:10', 'admin', '2019-07-20 20:13:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_category` VALUES ('621012154878922752', 'AWS', 'AWS认证相关', '-1', 30, NULL, 'admin', '2019-09-10 16:00:36', 'admin', '2019-09-10 16:00:36', 0, 'EXAM', 'gitee');

-- ----------------------------
-- Table structure for exam_subject_choices
-- ----------------------------
DROP TABLE IF EXISTS `exam_subject_choices`;
CREATE TABLE `exam_subject_choices`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `category_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类id',
  `serial_number` int(11) NULL DEFAULT NULL COMMENT '题目序号',
  `subject_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目名称',
  `choices_type` int(11) NULL DEFAULT NULL COMMENT '题目类型',
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参考答案',
  `score` int(11) NULL DEFAULT NULL COMMENT '题目分值',
  `analysis` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '解析',
  `level` int(11) NULL DEFAULT NULL COMMENT '难度等级',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '选择题表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_subject_choices
-- ----------------------------
INSERT INTO `exam_subject_choices` VALUES ('590970743878193152', '0', 1, '自计算机问世至今已经经历了四个时代，划分时代的主要依据是计算机的_', 0, 'D', 5, '<p>无</p>', 2, 'admin', '2019-06-19 18:26:45', 'admin', '2019-06-19 18:31:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590970887432441856', '0', 2, '第一台计算机是在1946年在美国诞生，该机的英文缩写是_', 0, 'A', 5, '<p>无</p>', 2, 'admin', '2019-06-19 18:27:19', 'admin', '2019-06-19 18:27:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590971026880466944', '0', 3, '当前的计算机一般被认为是第四代计算机，它所采用的逻辑元件是_', 0, 'C', 5, '无', 2, 'admin', '2019-06-19 18:27:52', 'admin', '2019-06-19 18:27:52', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590971151128334336', '0', 4, '当前计算机的应用最广泛的领域是_', 0, 'C', 5, '无', 2, 'admin', '2019-06-19 18:28:22', 'admin', '2019-06-19 18:28:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590971273539096576', '0', 5, '早期的计算机体积大、耗电多、速度慢，其主要原因是制约于_', 0, 'D', 5, '无', 2, 'admin', '2019-06-19 18:28:51', 'admin', '2019-06-19 18:28:51', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590971402639773696', '0', 6, '现代计算机之所以能够自动、连续地进行数据处理，主要是因为_', 0, 'D', 5, '无', 2, 'admin', '2019-06-19 18:29:22', 'admin', '2019-06-19 18:29:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590971508109742080', '0', 7, '电子计算机按规模和处理能力划分，可以分为_', 0, 'C', 5, '无', 2, 'admin', '2019-06-19 18:29:47', 'admin', '2019-06-19 18:29:47', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590971613944614912', '0', 8, '将计算机分为通用计算机、专用计算机两类的分类标准是_', 0, 'B', 5, '无', 2, 'admin', '2019-06-19 18:30:12', 'admin', '2019-06-19 18:30:12', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590971750146248704', '0', 9, '既可以接收、处理和输出模拟量，也可以接收、处理和输出数字量的计算机是_', 0, 'C', 5, '无', 2, 'admin', '2019-06-19 18:30:44', 'admin', '2019-06-19 18:30:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590971860041207808', '0', 10, '至今日，计算机仍采用存储程序原理，原理的提出者是_', 0, 'C', 5, '无', 2, 'admin', '2019-06-19 18:31:11', 'admin', '2019-06-19 18:31:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590972141118296064', '0', 1, '《小雅·鹿鸣》本是西周贵族宣扬宴飨之仪的乐歌，后扩散到民间，在乡人宴会上也可传唱。这表明西周时期', 0, 'B', 5, '无', 2, 'admin', '2019-06-19 18:32:18', 'admin', '2019-06-19 18:32:18', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590972250212143104', '0', 2, '公元前 212 年，秦始皇坑杀“术士”，长子扶苏劝谏说：“远方黔首未集，诸生皆诵法孔子，今上皆重法绳之，臣恐天下不安，唯上察之”。这反映当时', 0, 'D', 5, '', 2, 'admin', '2019-06-19 18:32:44', 'admin', '2019-06-19 18:32:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590972363110223872', '0', 3, '唐前期的政治人物多为北方人，北宋时政治人物多出生于江西、福建、苏南等地。这一变化主要反映了', 0, 'C', 5, '', 2, 'admin', '2019-06-19 18:33:11', 'admin', '2019-06-19 18:33:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590972473877598208', '0', 4, '李鸿章凭淮军实力日渐强盛。一次，他在游孔林时说道：“孔子不会打洋枪，今不足贵也。”李鸿章这样评价孔子，其背景最可能是', 0, 'B', 5, '', 2, 'admin', '2019-06-19 18:33:37', 'admin', '2019-06-19 18:33:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590972590177259520', '0', 5, '《荆楚岁时记》云：“社日，四邻并结宗会社，宰牲牢，为屋于树下，先祭神，然后食其胙。”据此可知，社日的民俗功能主要是', 0, 'A', 5, '', 2, 'admin', '2019-06-19 18:34:05', 'admin', '2019-06-19 18:34:05', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590972723384160256', '0', 6, '李鸿章凭淮军实力日渐强盛。一次，他在游孔林时说道：“孔子不会打洋枪，今不足贵也。”李鸿章这样评价孔子，其背景最可能是', 0, 'B', 5, '', 2, 'admin', '2019-06-19 18:34:36', 'admin', '2019-06-19 18:34:36', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590972834734542848', '0', 7, '1930 年，中共中央在回应共产国际指示时说：党的任务决不是准备夺取部分的政权，如果认为现在还是准备夺取部分的政权,无疑是对革命形势估量不足的右倾观念。这一回应表明', 0, 'D', 5, '', 2, 'admin', '2019-06-19 18:35:03', 'admin', '2019-06-19 18:35:03', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590972929693585408', '0', 8, '1938 年初，国民政府颁布多部法规，要求将每一工厂、商号、银行、钱庄都纳入到同业工会内，又将每一同业工会纳入当地商会内。这些法规', 0, 'D', 5, '', 2, 'admin', '2019-06-19 18:35:26', 'admin', '2019-06-19 18:35:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590973027995488256', '0', 9, '古代雅典法律规定：每个公民从出生起，城邦就是他的最高监护人，要按城邦的需要来抚养和教育。这反映出雅典', 0, 'A', 5, '', 2, 'admin', '2019-06-19 18:35:49', 'admin', '2019-06-19 18:35:49', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590973115157319680', '0', 10, '19 世纪末 20 世纪初，一向傲慢的英国人惊奇地发现：“身上的衣服是德国缝制的，少女们周末穿的漂亮披风与上衣来自德国。更让人吃惊的是生活中有许多东西都产自德国，……连周末歌剧院里上演的歌剧也是德国人创作的，演员无一例外是德国人。”可见，当时傲慢的英国人', 0, 'D', 5, '', 2, 'admin', '2019-06-19 18:36:10', 'admin', '2019-06-19 18:36:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590973253544185856', '0', 11, '1920–1921 年，苏俄许多工人流往农村，还有一些则自谋生路成了小手工业者。据此推知当时苏俄', 0, 'A', 5, '', 2, 'admin', '2019-06-19 18:36:43', 'admin', '2019-06-19 18:36:43', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590973360662515712', '0', 12, '1964 年 6 月，美国《时代》杂志发表社论指出：“从北约到联合国，从拉丁美洲到红色中国，几乎在世界政治中的每一个问题或地区上，法国都采取和美国政策不一致的态度。”这一社论', 0, 'B', 5, '', 2, 'admin', '2019-06-19 18:37:08', 'admin', '2019-06-19 18:37:08', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('590976455786237952', '590976335996915712', 1, '<p>自计算机问世至今已经经历了四个时代，划分时代的主要依据是计算机的_</p>', 0, 'D', 5, '', 2, 'admin', '2019-06-19 18:49:26', 'admin', '2019-07-20 20:29:15', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('602231589715447808', '602231546270846976', 1, '<p>32</p>', 3, 'A', 5, '233', 2, 'admin', '2019-07-20 20:13:20', 'admin', '2019-07-20 20:30:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('621013095883608064', '621012154878922752', 1, '<p>AWS的全称是什么？<img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"头像\" width=\"80\" height=\"80\" /></p>', 0, 'A', 5, '<p>亚马逊网络服务</p>', 2, 'admin', '2019-09-10 16:04:20', 'admin', '2019-09-11 10:03:14', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('621286411944988672', '621012154878922752', 2, '<p>AWS的全称是什么？<img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"=\" width=\"80\" height=\"80\" /></p>', 0, 'D', 5, '<p>AWS全称为Amazon Web Services,是亚马逊公司的云计算平台提供的一系列服务 AWS面向用户提供包括弹性计算、存储、数据库、应用程序在内的服务</p>', 2, 'admin', '2019-09-11 10:10:24', 'admin', '2019-09-11 10:10:24', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('621287044316008448', '', 2, '<p>AWS的全称是什么？</p>\n<p><img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"=\" width=\"80\" height=\"80\" /></p>', 0, 'D', 40, '<p>AWS全称为Amazon Web Services,是亚马逊公司的云计算平台提供的一系列服务 AWS面向用户提供包括弹性计算、存储、数据库、应用程序在内的服务</p>', 2, 'admin', '2019-09-11 10:12:54', 'admin', '2019-09-11 13:48:45', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('621300871069831168', '', 4, '<p>自计算机问世至今已经经历了四个时代，划分时代的主要依据是计算机的_</p>', 0, 'D', 10, '', 2, 'admin', '2019-09-11 11:07:51', 'admin', '2019-09-11 14:09:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('621346123818340352', '0', 1, '12222', 0, 'B', 50, '', 2, 'admin', '2019-09-11 14:07:40', 'admin', '2019-09-11 14:07:40', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('621347666936336384', '', 1, '<p>AWS的全称是什么？</p>\n<p><img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"头像\" width=\"80\" height=\"80\" /></p>', 0, 'A', 5, '<p>亚马逊网络服务</p>', 2, 'admin', '2019-09-11 14:13:48', 'admin', '2019-09-11 14:13:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('621347797207224320', '', 2, '<p>自计算机问世至今已经经历了四个时代，划分时代的主要依据是计算机的_</p>', 0, 'D', 5, '', 2, 'admin', '2019-09-11 14:14:19', 'admin', '2019-09-11 14:14:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_choices` VALUES ('621348039700910080', '', 3, '<p>AWS的全称是什么？</p>', 0, 'D', 5, '<p>AWS全称为Amazon Web Services,是亚马逊公司的云计算平台提供的一系列服务 AWS面向用户提供包括弹性计算、存储、数据库、应用程序在内的服务</p>', 2, 'admin', '2019-09-11 14:15:17', 'admin', '2019-09-11 14:17:27', 0, 'EXAM', 'gitee');

-- ----------------------------
-- Table structure for exam_subject_judgement
-- ----------------------------
DROP TABLE IF EXISTS `exam_subject_judgement`;
CREATE TABLE `exam_subject_judgement`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `category_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类ID',
  `serial_number` int(11) NULL DEFAULT NULL COMMENT '序号',
  `subject_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目名称',
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参考答案',
  `score` int(11) NULL DEFAULT NULL COMMENT '分值',
  `analysis` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '解析',
  `level` int(11) NULL DEFAULT NULL COMMENT '难度等级',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '简答题表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for exam_subject_option
-- ----------------------------
DROP TABLE IF EXISTS `exam_subject_option`;
CREATE TABLE `exam_subject_option`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `subject_choices_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选择题ID',
  `option_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项名称',
  `option_content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项内容',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '选择题选项表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of exam_subject_option
-- ----------------------------
INSERT INTO `exam_subject_option` VALUES ('590970743911747584', '590970743878193152', 'A', '<p>规模</p>', 'admin', '2019-06-19 18:26:45', 'admin', '2019-06-19 18:31:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590970743911747585', '590970743878193152', 'B', '<p>功能</p>', 'admin', '2019-06-19 18:26:45', 'admin', '2019-06-19 18:31:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590970743911747586', '590970743878193152', 'C', '<p>性能</p>', 'admin', '2019-06-19 18:26:45', 'admin', '2019-06-19 18:31:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590970743911747587', '590970743878193152', 'D', '构成元件', 'admin', '2019-06-19 18:26:45', 'admin', '2019-06-19 18:31:29', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590970887457607680', '590970887432441856', 'A', '<p>ENIAC</p>', 'admin', '2019-06-19 18:27:19', 'admin', '2019-06-19 18:27:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590970887457607681', '590970887432441856', 'B', '<p>EDVAC</p>', 'admin', '2019-06-19 18:27:19', 'admin', '2019-06-19 18:27:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590970887457607682', '590970887432441856', 'C', '<p>EDVAE</p>', 'admin', '2019-06-19 18:27:19', 'admin', '2019-06-19 18:27:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590970887457607683', '590970887432441856', 'D', 'MARK', 'admin', '2019-06-19 18:27:19', 'admin', '2019-06-19 18:27:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971026893049856', '590971026880466944', 'A', '<p>集成电路</p>', 'admin', '2019-06-19 18:27:52', 'admin', '2019-06-19 18:27:52', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971026901438464', '590971026880466944', 'B', '<p>晶体管</p>', 'admin', '2019-06-19 18:27:52', 'admin', '2019-06-19 18:27:52', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971026901438465', '590971026880466944', 'C', '<p>大规模集成电路</p>', 'admin', '2019-06-19 18:27:52', 'admin', '2019-06-19 18:27:52', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971026901438466', '590971026880466944', 'D', '电子管', 'admin', '2019-06-19 18:27:52', 'admin', '2019-06-19 18:27:52', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971151170277376', '590971151128334336', 'A', '<p>辅助设计</p>', 'admin', '2019-06-19 18:28:22', 'admin', '2019-06-19 18:28:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971151170277377', '590971151128334336', 'B', '<p>科学计算</p>', 'admin', '2019-06-19 18:28:22', 'admin', '2019-06-19 18:28:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971151170277378', '590971151128334336', 'C', '<p>数据处理</p>', 'admin', '2019-06-19 18:28:22', 'admin', '2019-06-19 18:28:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971151170277379', '590971151128334336', 'D', '<p>过程控制</p>', 'admin', '2019-06-19 18:28:22', 'admin', '2019-06-19 18:28:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971273560068096', '590971273539096576', 'A', '<p>元材料</p>', 'admin', '2019-06-19 18:28:51', 'admin', '2019-06-19 18:28:51', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971273560068097', '590971273539096576', 'B', '<p>工艺水平</p>', 'admin', '2019-06-19 18:28:51', 'admin', '2019-06-19 18:28:51', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971273560068098', '590971273539096576', 'C', '<p>设计水平</p>', 'admin', '2019-06-19 18:28:51', 'admin', '2019-06-19 18:28:51', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971273560068099', '590971273539096576', 'D', '元器件', 'admin', '2019-06-19 18:28:51', 'admin', '2019-06-19 18:28:51', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971402681716736', '590971402639773696', 'A', '<p>采用了开关电路</p>', 'admin', '2019-06-19 18:29:22', 'admin', '2019-06-19 18:29:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971402681716737', '590971402639773696', 'B', '<p>采用了半导体器件</p>', 'admin', '2019-06-19 18:29:22', 'admin', '2019-06-19 18:29:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971402681716738', '590971402639773696', 'C', '<p>采用了二进制</p>', 'admin', '2019-06-19 18:29:22', 'admin', '2019-06-19 18:29:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971402681716739', '590971402639773696', 'D', '<p>具有存储程序的功能</p>', 'admin', '2019-06-19 18:29:22', 'admin', '2019-06-19 18:29:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971508126519296', '590971508109742080', 'A', '<p>数字电子计算机和模拟电子计算机</p>', 'admin', '2019-06-19 18:29:47', 'admin', '2019-06-19 18:29:47', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971508126519297', '590971508109742080', 'B', '<p>通用计算机和专用计算机</p>', 'admin', '2019-06-19 18:29:47', 'admin', '2019-06-19 18:29:47', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971508126519298', '590971508109742080', 'C', '<p>巨型计算机、中小型计算机和微型计算机</p>', 'admin', '2019-06-19 18:29:47', 'admin', '2019-06-19 18:29:47', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971508126519299', '590971508109742080', 'D', '科学与过程计算计算机、工业控制计算机和数据计算机', 'admin', '2019-06-19 18:29:47', 'admin', '2019-06-19 18:29:47', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971613961392128', '590971613944614912', 'A', '<p>计算机处理数据的方式</p>', 'admin', '2019-06-19 18:30:12', 'admin', '2019-06-19 18:30:12', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971613961392129', '590971613944614912', 'B', '<p>计算机使用范围</p>', 'admin', '2019-06-19 18:30:12', 'admin', '2019-06-19 18:30:12', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971613961392130', '590971613944614912', 'C', '<p>机器的规模</p>', 'admin', '2019-06-19 18:30:12', 'admin', '2019-06-19 18:30:12', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971613961392131', '590971613944614912', 'D', '计算机的功能和处理能力', 'admin', '2019-06-19 18:30:12', 'admin', '2019-06-19 18:30:12', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971750163025920', '590971750146248704', 'A', '<p>电子数字计算机</p>', 'admin', '2019-06-19 18:30:44', 'admin', '2019-06-19 18:30:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971750167220224', '590971750146248704', 'B', '<p>电子模拟计算机</p>', 'admin', '2019-06-19 18:30:44', 'admin', '2019-06-19 18:30:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971750167220225', '590971750146248704', 'C', '<p>数模混合计算机</p>', 'admin', '2019-06-19 18:30:44', 'admin', '2019-06-19 18:30:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971750167220226', '590971750146248704', 'D', '<p>专用计算机</p>', 'admin', '2019-06-19 18:30:44', 'admin', '2019-06-19 18:30:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971860062179328', '590971860041207808', 'A', '<p>莫尔</p>', 'admin', '2019-06-19 18:31:11', 'admin', '2019-06-19 18:31:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971860062179329', '590971860041207808', 'B', '<p>比尔&middot;盖次</p>', 'admin', '2019-06-19 18:31:11', 'admin', '2019-06-19 18:31:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971860062179330', '590971860041207808', 'C', '<p>冯&middot;诺依曼</p>', 'admin', '2019-06-19 18:31:11', 'admin', '2019-06-19 18:31:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590971860062179331', '590971860041207808', 'D', '科得', 'admin', '2019-06-19 18:31:11', 'admin', '2019-06-19 18:31:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972141156044800', '590972141118296064', 'A', '<p>周人生活较为富足</p>', 'admin', '2019-06-19 18:32:18', 'admin', '2019-06-19 18:32:18', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972141156044801', '590972141118296064', 'B', '<p>礼乐文明得到广泛认同</p>', 'admin', '2019-06-19 18:32:18', 'admin', '2019-06-19 18:32:18', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972141156044802', '590972141118296064', 'C', '<p>乡人社会地位提高</p>', 'admin', '2019-06-19 18:32:18', 'admin', '2019-06-19 18:32:18', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972141156044803', '590972141118296064', 'D', '<p>贵族奢靡之风波及民间</p>', 'admin', '2019-06-19 18:32:18', 'admin', '2019-06-19 18:32:18', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972250224726016', '590972250212143104', 'A', '<p>统一进程曲折</p>', 'admin', '2019-06-19 18:32:44', 'admin', '2019-06-19 18:32:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972250224726017', '590972250212143104', 'B', '<p>地方治理不畅</p>', 'admin', '2019-06-19 18:32:44', 'admin', '2019-06-19 18:32:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972250224726018', '590972250212143104', 'C', '<p>始皇灭儒崇法</p>', 'admin', '2019-06-19 18:32:44', 'admin', '2019-06-19 18:32:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972250224726019', '590972250212143104', 'D', '儒学影响较大', 'admin', '2019-06-19 18:32:44', 'admin', '2019-06-19 18:32:44', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972363127001088', '590972363110223872', 'A', '<p>官僚集团重视本地域人才</p>', 'admin', '2019-06-19 18:33:11', 'admin', '2019-06-19 18:33:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972363127001089', '590972363110223872', 'B', '<p>南北方士人志向差异</p>', 'admin', '2019-06-19 18:33:11', 'admin', '2019-06-19 18:33:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972363127001090', '590972363110223872', 'C', '<p>科举制改变人才地域分布</p>', 'admin', '2019-06-19 18:33:11', 'admin', '2019-06-19 18:33:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972363127001091', '590972363110223872', 'D', '<p>政治中心转移到南方</p>', 'admin', '2019-06-19 18:33:11', 'admin', '2019-06-19 18:33:11', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972473902764032', '590972473877598208', 'A', '<p>&ldquo;师夷长技&rdquo;思想萌发</p>', 'admin', '2019-06-19 18:33:37', 'admin', '2019-06-19 18:33:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972473902764033', '590972473877598208', 'B', '<p>&ldquo;中体西用&rdquo;思潮兴起</p>', 'admin', '2019-06-19 18:33:37', 'admin', '2019-06-19 18:33:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972473902764034', '590972473877598208', 'C', '<p>&ldquo;中体西用&rdquo;思潮兴起</p>', 'admin', '2019-06-19 18:33:37', 'admin', '2019-06-19 18:33:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972473902764035', '590972473877598208', 'D', '<p>&ldquo;尊孔复古&rdquo;思潮泛滥</p>', 'admin', '2019-06-19 18:33:37', 'admin', '2019-06-19 18:33:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972590189842432', '590972590177259520', 'A', '<p>联谊乡邻</p>', 'admin', '2019-06-19 18:34:05', 'admin', '2019-06-19 18:34:05', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972590189842433', '590972590177259520', 'B', '<p>颂扬盛世</p>', 'admin', '2019-06-19 18:34:05', 'admin', '2019-06-19 18:34:05', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972590189842434', '590972590177259520', 'C', '<p>缅怀先祖</p>', 'admin', '2019-06-19 18:34:05', 'admin', '2019-06-19 18:34:05', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972590189842435', '590972590177259520', 'D', '助危济困', 'admin', '2019-06-19 18:34:05', 'admin', '2019-06-19 18:34:05', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972723392548864', '590972723384160256', 'A', '<p>&ldquo;师夷长技&rdquo;思想萌发</p>', 'admin', '2019-06-19 18:34:37', 'admin', '2019-06-19 18:34:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972723392548865', '590972723384160256', 'B', '<p>&ldquo;中体西用&rdquo;思潮兴起</p>', 'admin', '2019-06-19 18:34:37', 'admin', '2019-06-19 18:34:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972723392548866', '590972723384160256', 'C', '<p>&ldquo;托古改制&rdquo;思想产生</p>', 'admin', '2019-06-19 18:34:37', 'admin', '2019-06-19 18:34:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972723392548867', '590972723384160256', 'D', '<p>&ldquo;尊孔复古&rdquo;思潮泛滥</p>', 'admin', '2019-06-19 18:34:37', 'admin', '2019-06-19 18:34:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972834759708672', '590972834734542848', 'A', '<p>中共夺取全国政权时机成熟</p>', 'admin', '2019-06-19 18:35:03', 'admin', '2019-06-19 18:35:03', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972834759708673', '590972834734542848', 'B', '<p>中共找到符合国情的道路</p>', 'admin', '2019-06-19 18:35:03', 'admin', '2019-06-19 18:35:03', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972834759708674', '590972834734542848', 'C', '<p>中共找到符合国情的道路</p>', 'admin', '2019-06-19 18:35:03', 'admin', '2019-06-19 18:35:03', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972834759708675', '590972834734542848', 'D', '<p>中共出现自主革命的倾向</p>', 'admin', '2019-06-19 18:35:03', 'admin', '2019-06-19 18:35:03', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972929714556928', '590972929693585408', 'A', '<p>抑制了官僚资本膨胀</p>', 'admin', '2019-06-19 18:35:26', 'admin', '2019-06-19 18:35:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972929714556929', '590972929693585408', 'B', '<p>挫败了日军经济掠夺</p>', 'admin', '2019-06-19 18:35:26', 'admin', '2019-06-19 18:35:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972929714556930', '590972929693585408', 'C', '<p>防止了国民经济崩溃</p>', 'admin', '2019-06-19 18:35:26', 'admin', '2019-06-19 18:35:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590972929714556931', '590972929693585408', 'D', '积聚了抗战经济力量', 'admin', '2019-06-19 18:35:26', 'admin', '2019-06-19 18:35:26', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973028020654080', '590973027995488256', 'A', '<p>公民培养受强制性约束</p>', 'admin', '2019-06-19 18:35:49', 'admin', '2019-06-19 18:35:49', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973028020654081', '590973027995488256', 'B', '<p>法律有违人文精神</p>', 'admin', '2019-06-19 18:35:49', 'admin', '2019-06-19 18:35:49', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973028020654082', '590973027995488256', 'C', '<p>父母失去教育子女资格</p>', 'admin', '2019-06-19 18:35:49', 'admin', '2019-06-19 18:35:49', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973028020654083', '590973027995488256', 'D', '公民教育制度完备', 'admin', '2019-06-19 18:35:49', 'admin', '2019-06-19 18:35:49', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973115178291200', '590973115157319680', 'A', '<p>推崇德国文化</p>', 'admin', '2019-06-19 18:36:10', 'admin', '2019-06-19 18:36:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973115178291201', '590973115157319680', 'B', '<p>注意到了德国的威胁</p>', 'admin', '2019-06-19 18:36:10', 'admin', '2019-06-19 18:36:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973115178291202', '590973115157319680', 'C', '<p>喜爱德国产品</p>', 'admin', '2019-06-19 18:36:10', 'admin', '2019-06-19 18:36:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973115178291203', '590973115157319680', 'D', '意识到了德国的崛起', 'admin', '2019-06-19 18:36:10', 'admin', '2019-06-19 18:36:10', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973253556768768', '590973253544185856', 'A', '<p>战时共产主义政策有所突破</p>', 'admin', '2019-06-19 18:36:43', 'admin', '2019-06-19 18:36:43', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973253556768769', '590973253544185856', 'B', '<p>新经济政策成效显著</p>', 'admin', '2019-06-19 18:36:43', 'admin', '2019-06-19 18:36:43', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973253556768770', '590973253544185856', 'C', '<p>国营农庄吸引了大量劳动力</p>', 'admin', '2019-06-19 18:36:43', 'admin', '2019-06-19 18:36:43', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973253556768771', '590973253544185856', 'D', '政府重视日用品生产', 'admin', '2019-06-19 18:36:43', 'admin', '2019-06-19 18:36:43', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973360691875840', '590973360662515712', 'A', '<p>揭示了法国倒向社会主义阵营</p>', 'admin', '2019-06-19 18:37:08', 'admin', '2019-06-19 18:37:08', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973360691875841', '590973360662515712', 'B', '<p>反映了法国推行独立外交</p>', 'admin', '2019-06-19 18:37:08', 'admin', '2019-06-19 18:37:08', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973360691875842', '590973360662515712', 'C', '<p>体现了两大阵营对抗趋于缓和</p>', 'admin', '2019-06-19 18:37:08', 'admin', '2019-06-19 18:37:08', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590973360691875843', '590973360662515712', 'D', '体现了两大阵营对抗趋于缓和', 'admin', '2019-06-19 18:37:08', 'admin', '2019-06-19 18:37:08', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590976455823986688', '590976455786237952', 'A', '<p>规模</p>', 'admin', '2019-06-19 18:49:26', 'admin', '2019-07-20 20:29:15', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590976455823986689', '590976455786237952', 'B', '<p>功能</p>', 'admin', '2019-06-19 18:49:26', 'admin', '2019-07-20 20:29:15', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590976455823986690', '590976455786237952', 'C', '<p>性能</p>', 'admin', '2019-06-19 18:49:26', 'admin', '2019-07-20 20:29:15', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590976455823986691', '590976455786237952', 'D', '构成元件', 'admin', '2019-06-19 18:49:26', 'admin', '2019-07-20 20:29:15', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590976456683819008', '590976456675430400', 'A', '<p>规模</p>', 'admin', '2019-06-19 18:49:27', 'admin', '2019-06-19 18:49:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590976456683819009', '590976456675430400', 'B', '<p>功能</p>', 'admin', '2019-06-19 18:49:27', 'admin', '2019-06-19 18:49:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590976456683819010', '590976456675430400', 'C', '<p>性能</p>', 'admin', '2019-06-19 18:49:27', 'admin', '2019-06-19 18:49:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('590976456683819011', '590976456675430400', 'D', '构成元件', 'admin', '2019-06-19 18:49:27', 'admin', '2019-06-19 18:49:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('602231589723836416', '602231589715447808', 'A', '32', 'admin', '2019-07-20 20:13:20', 'admin', '2019-07-20 20:30:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('602231589723836417', '602231589715447808', 'B', '323', 'admin', '2019-07-20 20:13:20', 'admin', '2019-07-20 20:30:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('602231589723836418', '602231589715447808', 'C', '323', 'admin', '2019-07-20 20:13:20', 'admin', '2019-07-20 20:30:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('602231589723836419', '602231589715447808', 'D', '323', 'admin', '2019-07-20 20:13:20', 'admin', '2019-07-20 20:30:22', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621013096009437184', '621013095883608064', 'A', '<p>Amazon Web Services</p>', 'admin', '2019-09-10 16:04:20', 'admin', '2019-09-11 10:03:14', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621013096009437185', '621013095883608064', 'B', '<p>Amazon Web Service</p>', 'admin', '2019-09-10 16:04:20', 'admin', '2019-09-11 10:03:14', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621013096009437186', '621013095883608064', 'C', '<p>Admin Web Service</p>', 'admin', '2019-09-10 16:04:20', 'admin', '2019-09-11 10:03:14', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621013096009437187', '621013095883608064', 'D', '<p>Admin Web Services</p>', 'admin', '2019-09-10 16:04:20', 'admin', '2019-09-11 10:03:14', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621286411953377280', '621286411944988672', 'A', '<p>AWS</p>', 'admin', '2019-09-11 10:10:24', 'admin', '2019-09-11 10:10:24', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621286411953377281', '621286411944988672', 'B', '<p>212121211111111111121111111111111111111111<img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" width=\"80\" height=\"80\" /></p>', 'admin', '2019-09-11 10:10:24', 'admin', '2019-09-11 10:10:24', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621286411953377282', '621286411944988672', 'C', '<p>212121<img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" width=\"80\" height=\"80\" /></p>', 'admin', '2019-09-11 10:10:24', 'admin', '2019-09-11 10:10:24', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621286411953377283', '621286411944988672', 'D', '<p>Amazon Web Services</p>', 'admin', '2019-09-11 10:10:24', 'admin', '2019-09-11 10:10:24', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621287044328591360', '621287044316008448', 'A', '<p>AWS</p>', 'admin', '2019-09-11 10:12:54', 'admin', '2019-09-11 13:48:45', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621287044328591361', '621287044316008448', 'B', '<p>212121211111111111121111111111111111111111<img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" width=\"80\" height=\"80\" /></p>', 'admin', '2019-09-11 10:12:54', 'admin', '2019-09-11 13:48:45', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621287044328591362', '621287044316008448', 'C', '<p>212121<img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" width=\"80\" height=\"80\" /></p>', 'admin', '2019-09-11 10:12:54', 'admin', '2019-09-11 13:48:45', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621287044328591363', '621287044316008448', 'D', '<p>Amazon Web Services</p>', 'admin', '2019-09-11 10:12:54', 'admin', '2019-09-11 13:48:45', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621300871086608384', '621300871069831168', 'A', '<p>规模</p>', 'admin', '2019-09-11 11:07:51', 'admin', '2019-09-11 14:09:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621300871086608385', '621300871069831168', 'B', '<p>功能</p>', 'admin', '2019-09-11 11:07:51', 'admin', '2019-09-11 14:09:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621300871086608386', '621300871069831168', 'C', '<p>性能</p>', 'admin', '2019-09-11 11:07:51', 'admin', '2019-09-11 14:09:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621300871086608387', '621300871069831168', 'D', '构成元件', 'admin', '2019-09-11 11:07:51', 'admin', '2019-09-11 14:09:37', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621346123835117568', '621346123818340352', 'A', '<p>22111<a href=\"http://www.baidu.com\" target=\"_blank\" rel=\"noopener\">百度一下</a></p>', 'admin', '2019-09-11 14:07:40', 'admin', '2019-09-11 14:07:40', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621346123835117569', '621346123818340352', 'B', '11111111', 'admin', '2019-09-11 14:07:40', 'admin', '2019-09-11 14:07:40', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621346123835117570', '621346123818340352', 'C', '11111111', 'admin', '2019-09-11 14:07:40', 'admin', '2019-09-11 14:07:40', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621346123835117571', '621346123818340352', 'D', '<p><a href=\"http://www.baidu.com\" target=\"_blank\" rel=\"noopener\">www.baidu.com</a></p>', 'admin', '2019-09-11 14:07:40', 'admin', '2019-09-11 14:07:40', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621347666957307904', '621347666936336384', 'A', '<p>Amazon Web Services</p>', 'admin', '2019-09-11 14:13:48', 'admin', '2019-09-11 14:13:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621347666957307905', '621347666936336384', 'B', '<p>Amazon Web Service</p>', 'admin', '2019-09-11 14:13:48', 'admin', '2019-09-11 14:13:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621347666957307906', '621347666936336384', 'C', '<p>Admin Web Service</p>', 'admin', '2019-09-11 14:13:48', 'admin', '2019-09-11 14:13:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621347666957307907', '621347666936336384', 'D', '<p>Admin Web Services</p>', 'admin', '2019-09-11 14:13:48', 'admin', '2019-09-11 14:13:48', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621347797224001536', '621347797207224320', 'A', '<p>规模</p>', 'admin', '2019-09-11 14:14:19', 'admin', '2019-09-11 14:14:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621347797224001537', '621347797207224320', 'B', '<p>功能</p>', 'admin', '2019-09-11 14:14:19', 'admin', '2019-09-11 14:14:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621347797224001538', '621347797207224320', 'C', '<p>性能</p>', 'admin', '2019-09-11 14:14:19', 'admin', '2019-09-11 14:14:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621347797224001539', '621347797207224320', 'D', '构成元件', 'admin', '2019-09-11 14:14:19', 'admin', '2019-09-11 14:14:19', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621348039717687296', '621348039700910080', 'A', '<p>AWS</p>', 'admin', '2019-09-11 14:15:17', 'admin', '2019-09-11 14:17:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621348039717687297', '621348039700910080', 'B', '<p>212121211111111111121111111111111111111111</p>\n<p><img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" width=\"80\" height=\"80\" /></p>', 'admin', '2019-09-11 14:15:17', 'admin', '2019-09-11 14:17:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621348039717687298', '621348039700910080', 'C', '<p>212121</p>\n<p><img src=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" alt=\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif?imageView2/1/w/80/h/80\" width=\"80\" height=\"80\" /></p>', 'admin', '2019-09-11 14:15:17', 'admin', '2019-09-11 14:17:27', 0, 'EXAM', 'gitee');
INSERT INTO `exam_subject_option` VALUES ('621348039717687299', '621348039700910080', 'D', '<p>Amazon Web Services</p>', 'admin', '2019-09-11 14:15:17', 'admin', '2019-09-11 14:17:27', 0, 'EXAM', 'gitee');

-- ----------------------------
-- Table structure for exam_subject_short_answer
-- ----------------------------
DROP TABLE IF EXISTS `exam_subject_short_answer`;
CREATE TABLE `exam_subject_short_answer`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `category_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类ID',
  `serial_number` int(11) NULL DEFAULT NULL COMMENT '序号',
  `subject_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目名称',
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参考答案',
  `score` int(11) NULL DEFAULT NULL COMMENT '分值',
  `analysis` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '解析',
  `level` int(11) NULL DEFAULT NULL COMMENT '难度等级',
  `creator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_date` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `modifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `modify_date` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `del_flag` int(11) NULL DEFAULT 0 COMMENT '删除标记 0:正常;1:删除',
  `application_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统编号',
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租户编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '简答题表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
