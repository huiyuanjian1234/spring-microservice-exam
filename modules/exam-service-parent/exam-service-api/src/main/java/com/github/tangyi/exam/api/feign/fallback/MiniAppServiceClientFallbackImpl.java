package com.github.tangyi.exam.api.feign.fallback;

import com.github.tangyi.common.core.utils.Result;
import com.github.tangyi.exam.api.feign.MiniAppServiceClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: MiniAppServiceClientFallbackImpl
 * @Auther: hyj
 * @Date: 2019/10/10 11:15
 */
@Slf4j
@Service
public class MiniAppServiceClientFallbackImpl implements MiniAppServiceClient {

    private Throwable throwable;
    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public Result saveRecordAndIntegral(Map<String, Object> map) {
        log.error("调用{}异常, {}， {}", "saveRecordAndIntegral", throwable);
        return Result.error("调用异常");
    }
}
