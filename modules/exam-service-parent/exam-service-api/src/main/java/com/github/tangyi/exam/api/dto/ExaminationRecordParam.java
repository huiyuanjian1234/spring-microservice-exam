package com.github.tangyi.exam.api.dto;

import com.github.tangyi.exam.api.module.ExaminationRecord;
import lombok.Data;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: ExaminationRecordParam
 * @Auther: hyj
 * @Date: 2019/9/18 18:10
 */
@Data
public class ExaminationRecordParam extends ExaminationRecord {

    /**
     * 考试类型 one:每日一题 ten: 每日十题
     */
    private String examType;
}
