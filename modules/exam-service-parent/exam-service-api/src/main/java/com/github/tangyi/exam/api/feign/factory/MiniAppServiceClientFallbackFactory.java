package com.github.tangyi.exam.api.feign.factory;

import com.github.tangyi.exam.api.feign.MiniAppServiceClient;
import com.github.tangyi.exam.api.feign.fallback.MiniAppServiceClientFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: MiniAppServiceClientFallbackFactory
 * @Auther: hyj
 * @Date: 2019/10/10 11:13
 */
@Component
public class MiniAppServiceClientFallbackFactory implements FallbackFactory<MiniAppServiceClient> {

    @Override
    public MiniAppServiceClient create(Throwable throwable) {
        MiniAppServiceClientFallbackImpl miniAppServiceClientFallback = new MiniAppServiceClientFallbackImpl();
        miniAppServiceClientFallback.setThrowable(throwable);
        return miniAppServiceClientFallback;
    }
}
