package com.github.tangyi.exam.api.module;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.tangyi.common.core.persistence.BaseEntity;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: AppHost
 * @Auther: hyj
 * @Date: 2019/10/17 18:46
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppHost extends BaseEntity<AppHost> {
    private String id; // 主键id
    /**
     * 域名
     */
    private String host;

    /**
     * 端口
     */
    private String port;

    /**
     * 备注
     */
    private String remark;

    /**
     * 启用状态
     */
    private String status;

}
