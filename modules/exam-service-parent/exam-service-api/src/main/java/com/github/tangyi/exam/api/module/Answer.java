package com.github.tangyi.exam.api.module;

import com.github.tangyi.common.core.persistence.BaseEntity;
import lombok.Data;

/**
 * 答题
 *
 * @author tangyi
 * @date 2018/11/8 20:59
 */
@Data
public class Answer extends BaseEntity<Answer> {

    /**
     * 考试记录id
     */
    private String examRecordId;

    /**
     * 考试id
     */
    private String examinationId;

    /**
     * 题目标签
     */
    private String subjectTag;

    /**
     * 考试类型
     */
    private String examType;

    /**
     * 小程序formId
     */
    private String formId;

    /**
     * 考试名称
     */
    private String examinationName;

    /**
     * 考试序号
     */
    private String examNumber;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 题目ID
     */
    private String subjectId;

    /**
     * 题目类型 0选择题，1简答题
     */
    private Integer type;

    /**
     * 答案
     */
    private String answer;

    /**
     * 答题类型，0：正确，1：错误
     */
    private Integer answerType;

    /**
     * 得分
     */
    private Integer score;

    /**
     * 批改状态
     */
    private Integer markStatus;

    /**
     * 题目序号
     */
    private Integer serialNumber;
}
