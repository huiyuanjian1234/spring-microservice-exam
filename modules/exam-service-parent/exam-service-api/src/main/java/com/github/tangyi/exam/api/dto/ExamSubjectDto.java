package com.github.tangyi.exam.api.dto;

import com.github.tangyi.exam.api.module.Examination;
import lombok.Data;

import java.io.Serializable;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: ExamSubjectDto
 * @Auther: hyj
 * @Date: 2019/9/18 11:33
 */

@Data
public class ExamSubjectDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 考试信息
     */
    private Examination examination;

    /**
     * 题目信息
     */
    private SubjectDto subjectDto;
}


