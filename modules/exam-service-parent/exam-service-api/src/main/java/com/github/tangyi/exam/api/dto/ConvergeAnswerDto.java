package com.github.tangyi.exam.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: ConvergeAnswerDto
 * @Auther: hyj
 * @Date: 2019/10/12 14:56
 */
@Data
public class ConvergeAnswerDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 考试id
     */
    private String examinationId;

    /**
     * 考试记录id
     */
    private String examRecordId;

    /**
     * 考试序号
     */
    private String examNumber;

    /**
     * 小程序formId
     */
    private String formId;

    /**
     * 题目ID
     */
    private String subjectId;

    /**
     * 题目标签
     */
    private String subjectTag;

    /**
     * 我的答案
     */
    private String myAnswer;

    /**
     * 参考答案
     */
    private String rightAnswer;

    /**
     * 答案类型 0 正确 1：错误
     */
    private Integer answerType;

    /**
     * 考试类型 1 每日一题 2：10题连答
     */
    private Integer examType;

    /**
     * 创建时间
     */
    private String createTime;

}
