package com.github.tangyi.exam.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author tangyi
 * @date 2019/1/9 20:58
 */
@Data
public class SubjectsDto implements Serializable {
    /**
     * 选项列表
     */
    private List<SubjectDto> subjectDto;
}
