package com.github.tangyi.exam.api.feign;

import com.github.tangyi.common.core.utils.Result;
import com.github.tangyi.exam.api.feign.factory.MiniAppServiceClientFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: MiniAppServiceClient
 * @Auther: hyj
 * @Date: 2019/10/10 11:12
 */
@FeignClient(url = "http://192.168.200.192", name = "miniapp", fallbackFactory = MiniAppServiceClientFallbackFactory.class)
public interface MiniAppServiceClient {
    /**
     * 保存答题信息和积分接口：
     * 192.168.200.192/freelancer-miniapp/exam-record/saveRecordAndIntegral

     * map里参数：
     *  mapData.put("userId",answer.getUserId());
     *  mapData.put("examRecordId",recordId);
     *  mapData.put("examinationId",examinationId);
     *  mapData.put("subjectId",answer.getSubjectId());
     *  mapData.put("subjectTag",answer.getSubjectTag());
     *  mapData.put("formId",answer.getFormId());
     *  mapData.put("examType",1);   // 1：每日1题   2：十题代练
     *  mapData.put("answerType",1); // 1: 错误      0：正确
     *  mapData.put("integral","5");
     *  mapData.put("createDate",new Date( ));

     * 用户id： userId  （String 长度64）
     * 考试记录id：examRecordId （String 长度64）
     * 考试id： examinationId （String 长度64 ）
     * 试题id：subjectId （String 长度64）
     * 小程序  formId：formId （String 长度40）
     * 考试类型：examType  （int，1：每日1题，2：十题代练）
     * 答案类型：answerType （int，0：错误，1：正确）
     * 积分：integral (String)
     * 创建日期：createDate (Date)
     *
     * @param map
     * @return
     */
    @RequestMapping(value="/freelancer-miniapp/exam-record/saveRecordAndIntegral", method= RequestMethod.POST)
    Result saveRecordAndIntegral(@RequestBody Map<String, Object> map);

}
