package com.github.tangyi.exam.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: SubjectHistoryDto
 * @Auther: hyj
 * @Date: 2019/10/9 16:19
 */
@Data
public class SubjectHistoryDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 考试ID
     */
    private String examinationId;

    /**
     * 考试记录ID
     */
    private String examRecordId;

    /**
     * 考题id
     */
    private String subjectId;

    /**
     * 用户id
     */
    private String userId;
}
