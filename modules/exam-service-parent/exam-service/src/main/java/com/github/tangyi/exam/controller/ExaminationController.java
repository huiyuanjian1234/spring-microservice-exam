package com.github.tangyi.exam.controller;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.pagehelper.PageInfo;
import com.github.tangyi.common.core.constant.CommonConstant;
import com.github.tangyi.common.core.model.ResponseBean;
import com.github.tangyi.common.core.utils.PageUtil;
import com.github.tangyi.common.core.utils.Result;
import com.github.tangyi.common.core.utils.SysUtil;
import com.github.tangyi.common.core.web.BaseController;
import com.github.tangyi.common.log.annotation.Log;
import com.github.tangyi.common.security.constant.SecurityConstant;
import com.github.tangyi.exam.api.dto.*;
import com.github.tangyi.exam.api.module.*;
import com.github.tangyi.exam.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 考试controller
 * @author tangyi
 * @date 2018/11/8 21:26
 */
@Slf4j
@AllArgsConstructor
@Api("考试信息管理")
@RestController
@RequestMapping("/v1/examination")
public class ExaminationController extends BaseController {

    @Autowired
    private ObjectMapper objectMapper;
    private final ExaminationService examinationService;
    private final CourseService courseService;
    private final AnswerService answerService;
    private final SubjectService subjectService;
    private final SubjectChoicesService subjectChoicesService;
    private final ExaminationSubjectService examinationSubjectService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    public static final String EVERYDAY_EXAM_RECORD_CACHE_PREFIX = "EveryDayExamRecord:";


    /**
     * 根据ID获取
     * @param id id
     * @return ResponseBean
     * @author tangyi
     * @date 2018/11/10 21:08
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "获取考试信息", notes = "根据考试id获取考试详细信息")
    @ApiImplicitParam(name = "id", value = "考试ID", required = true, dataType = "String", paramType = "path")
    public ResponseBean<Examination> examination(@PathVariable String id) {
        Examination examination = new Examination();
        examination.setId(id);
        return new ResponseBean<>(examinationService.get(examination));
    }

    /**
     * 获取分页数据
     * @param pageNum     pageNum
     * @param pageSize    pageSize
     * @param sort        sort
     * @param order       order
     * @param examination examination
     * @return PageInfo
     * @author tangyi
     * @date 2018/11/10 21:10
     */
    @GetMapping("examinationList")
    @ApiOperation(value = "获取考试列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = CommonConstant.PAGE_NUM, value = "分页页码", defaultValue = CommonConstant.PAGE_NUM_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.PAGE_SIZE, value = "分页大小", defaultValue = CommonConstant.PAGE_SIZE_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.SORT, value = "排序字段", defaultValue = CommonConstant.PAGE_SORT_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.ORDER, value = "排序方向", defaultValue = CommonConstant.PAGE_ORDER_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = "examination", value = "考试信息", dataType = "Examination")
    })
    public PageInfo<ExaminationDto> examinationList(@RequestParam(value = CommonConstant.PAGE_NUM, required = false, defaultValue = CommonConstant.PAGE_NUM_DEFAULT) String pageNum,
                                                    @RequestParam(value = CommonConstant.PAGE_SIZE, required = false, defaultValue = CommonConstant.PAGE_SIZE_DEFAULT) String pageSize,
                                                    @RequestParam(value = CommonConstant.SORT, required = false, defaultValue = CommonConstant.PAGE_SORT_DEFAULT) String sort,
                                                    @RequestParam(value = CommonConstant.ORDER, required = false, defaultValue = CommonConstant.PAGE_ORDER_DEFAULT) String order,
                                                    Examination examination) {
        examination.setTenantCode(SysUtil.getTenantCode());
        PageInfo<Examination> page = examinationService.findPage(PageUtil.pageInfo(pageNum, pageSize, sort, order), examination);
        PageInfo<ExaminationDto> examinationDtoPageInfo = new PageInfo<>();
        BeanUtils.copyProperties(page, examinationDtoPageInfo);
        if (CollectionUtils.isNotEmpty(page.getList())) {
            Course course = new Course();
            // 流处理获取课程ID集合，转成字符串数组
            course.setIds(page.getList().stream().map(Examination::getCourseId).distinct().toArray(String[]::new));
            List<Course> courses = courseService.findListById(course);
            // 流处理转成Dto集合
            List<ExaminationDto> examinationDtos = page.getList().stream().map(exam -> {
                ExaminationDto examinationDto = new ExaminationDto();
                BeanUtils.copyProperties(exam, examinationDto);
                // 设置考试所属课程
                courses.stream().filter(tempCourse -> tempCourse.getId().equals(exam.getCourseId())).findFirst().ifPresent(examinationDto::setCourse);
                return examinationDto;
            }).collect(Collectors.toList());
            examinationDtoPageInfo.setList(examinationDtos);
        }
        return examinationDtoPageInfo;
    }

    /**
     * 根据考试ID获取题目分页数据
     * @param pageNum    pageNum
     * @param pageSize   pageSize
     * @param sort       sort
     * @param order      order
     * @param subjectDto subjectDto
     * @return PageInfo
     * @author tangyi
     * @date 2019/6/16 15:45
     */
    @RequestMapping("/{examinationId}/subjectList")
    @ApiOperation(value = "获取题目列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = CommonConstant.PAGE_NUM, value = "分页页码", defaultValue = CommonConstant.PAGE_NUM_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.PAGE_SIZE, value = "分页大小", defaultValue = CommonConstant.PAGE_SIZE_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.SORT, value = "排序字段", defaultValue = CommonConstant.PAGE_SORT_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.ORDER, value = "排序方向", defaultValue = CommonConstant.PAGE_ORDER_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = "examinationId", value = "考试ID", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "subjectDto", value = "题目信息", dataType = "SubjectDto")
    })
    public PageInfo<SubjectDto> subjectList(@RequestParam(value = CommonConstant.PAGE_NUM, required = false, defaultValue = CommonConstant.PAGE_NUM_DEFAULT) String pageNum,
                                            @RequestParam(value = CommonConstant.PAGE_SIZE, required = false, defaultValue = CommonConstant.PAGE_SIZE_DEFAULT) String pageSize,
                                            @RequestParam(value = CommonConstant.SORT, required = false, defaultValue = CommonConstant.PAGE_SORT_DEFAULT) String sort,
                                            @RequestParam(value = CommonConstant.ORDER, required = false, defaultValue = CommonConstant.PAGE_ORDER_DEFAULT) String order,
                                            @PathVariable String examinationId, SubjectDto subjectDto) {
        subjectDto.setExaminationId(examinationId);
        return examinationService.findSubjectPageById(subjectDto, pageNum, pageSize, sort, order);
    }


    /**
     * 创建
     * @param examinationDto examinationDto
     * @return ResponseBean
     * @author tangyi
     * @date 2018/11/10 21:14
     */
    @PostMapping
    @PreAuthorize("hasAuthority('exam:exam:add') or hasAnyRole('" + SecurityConstant.ROLE_ADMIN + "')")
    @ApiOperation(value = "创建考试", notes = "创建考试")
    @ApiImplicitParam(name = "examinationDto", value = "考试实体examinationDto", required = true, dataType = "ExaminationDto")
    @Log("新增考试")
    public ResponseBean<Boolean> addExamination(@RequestBody @Valid ExaminationDto examinationDto) {
        Examination examination = new Examination();
        BeanUtils.copyProperties(examinationDto, examination);
        examination.setCourseId(examinationDto.getCourse().getId());
        examination.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
        return new ResponseBean<>(examinationService.insert(examination) > 0);
    }

    /**
     * 创建
     * @param examinationDto examinationDto
     * @return ResponseBean
     * @author huiyuanjian
     * @date 2019/09/18 17:56
     */
    @PostMapping("saveExamination")
    @ApiOperation(value = "保存考试", notes = "创建考试")
    @ApiImplicitParam(name = "examinationDto", value = "考试实体examinationDto", required = true, dataType = "ExaminationDto")
    @Log("新增考试")
    public ResponseBean<Boolean> saveExamination(@RequestBody @Valid ExaminationDto examinationDto) {
        String typeFlag = examinationDto.getExamType()+"-";
        Examination examination = new Examination();
        BeanUtils.copyProperties(examinationDto, examination);
        examination.setCourseId(examinationDto.getCourse().getId());

        Long time = System.currentTimeMillis()-24*60*60*1000;//获取当前时间精确到毫秒级的时间戳，例：1525849325942

        examination.setExamNumber(typeFlag+SysUtil.timeStamp2Date(time));

        // 通过examNumber获取前一天的考试记录
        Examination examinationByNumber = examinationService.findExaminationByExamNumber(examination);

        log.debug("autoNumber:"+examinationByNumber.getAutoNumber());
        log.debug("oldExamNumber:"+examinationByNumber.getExamNumber());

        examination.setExamNumber(typeFlag+SysUtil.timeStamp2Date(System.currentTimeMillis()));

        if ("one-".equals(typeFlag)) {
            examination.setAutoNumber(examinationByNumber.getAutoNumber()+1);
        } else {
            examination.setAutoNumber(examinationByNumber.getAutoNumber()+10);
        }

        log.debug("newExamNumber:"+examination.getExamNumber());
        String currentUsername = "", applicationCode = SysUtil.getSysCode(), tenantCode = SysUtil.getTenantCode();

        // 获取用户名
        if (!StringUtils.isBlank(examinationDto.getCreator())){
            currentUsername = examinationDto.getCreator();
        }else{
            currentUsername = SysUtil.getUser();
        }

        examination.setCommonValue(currentUsername, applicationCode, tenantCode);
        examinationService.insert(examination);

        int resut = 0;
        if ( "one-".equals(typeFlag) ){
            resut = examinationService.autoCreateExamination(examination,1);
        } else {
            resut = examinationService.autoCreateExamination(examination,10);
        }
        return new ResponseBean<>(resut > 0);

    }

    public ExaminationDto createExamParam () {
        ExaminationDto examinationDto = new ExaminationDto();
        Course course = new Course();
        long startTIme = System.currentTimeMillis();
        long endTime = System.currentTimeMillis() + 24*60*60*1000;
        course.setId("590968789617741824");
        course.setCourseName("计算机基础");
        examinationDto.setStartTime(new Date(startTIme));
        examinationDto.setEndTime(new Date(endTime));
        examinationDto.setStatus("0");
        examinationDto.setCourse(course);
        examinationDto.setCreator("system");
        return examinationDto;
    }

    /**
     * 自动创建每日一题考试
     */
    @Scheduled(cron = "1 0 0 * * ?")
    @PostMapping("createOneExamination")
    public void autoCreateOneExam () {
        ExaminationDto examinationDto = createExamParam();
        examinationDto.setExamType("one");
        examinationDto.setId("");
        examinationDto.setExaminationName(SysUtil.timeStamp2Date(System.currentTimeMillis())+"-每日一题");
        examinationDto.setTotalScore("10");//设置总分
        examinationDto.setType("0");
        saveExamination(examinationDto);
        log.info(SysUtil.timeStamp2Date(System.currentTimeMillis())+" 每日一题考试卷系统自动创建成功！");
    }

    //@Scheduled(cron = "1 0 0 * * ?")
    @PostMapping("createTenExamination")
    public void autoCreateTenExam () {
        ExaminationDto examinationDto = createExamParam();
        examinationDto.setExamType("ten");
        examinationDto.setId("");
        examinationDto.setExaminationName(SysUtil.timeStamp2Date(System.currentTimeMillis())+"-10题连答");
        examinationDto.setTotalScore("100");
        examinationDto.setType("0");
        saveExamination(examinationDto);
        log.info(SysUtil.timeStamp2Date(System.currentTimeMillis())+" 10题连答考试卷系统自动创建成功！");
    }

    //更新考试autoNumber序号
    @Scheduled(cron = "1 1 0 * * ?")
    @PostMapping("updateAutoExamNumber")
    public void updateAutoExamNumber () {
        log.info("执行自动更新autoNumber方法开始");
        String typeFlag = "one-";
        Examination examination = new Examination();
        Long time = System.currentTimeMillis();//获取当前时间精确到毫秒级的时间戳，例：1525849325942
        examination.setExamNumber(typeFlag+SysUtil.timeStamp2Date(time));

        // 通过examNumber获取当天的考试记录
        Examination dBExamination = examinationService.findExaminationByExamNumber(examination);
        log.debug("autoNumber:"+dBExamination.getAutoNumber());

        if(dBExamination == null){
            log.error("未查到该考试！");
            return;
        }

        Integer autoNumber = dBExamination.getAutoNumber();

        log.info("当前autoNumber:{}",autoNumber);

        //查询本套试卷的题目总数
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setExaminationId(dBExamination.getId());
        int totalSubject = examinationSubjectService.findSubjectCount(examinationSubject);
        log.debug("totalSubject:{}", totalSubject);

        //题库中总题数不包括分类为0
        SubjectChoices subjectChoices = new SubjectChoices();
        int totalSubjectCount = subjectChoicesService.findTotalSubjectCount(subjectChoices);
        log.debug("totalSubjectCount:{}", totalSubjectCount);

        if(autoNumber >= totalSubjectCount){
            dBExamination.setAutoNumber(1);
            int result = examinationService.update(dBExamination);
            if(result>0){
                log.info("自动更新考试autoNumber为1成功！");
            }else {
                log.error("自动更新考试autoNumber为1失败！");
            }
        }
        log.info("执行自动更新autoNumber方法结束！");

    }



    /**
     * 获取每日考试（未登录）
     * @param examRecord everyDayOneExercises
     * @return ResponseBean
     * @author huiyuanjian
     * @date 2019/04/30 16:45
     */
    @PostMapping("everyDayExam")
    @Log("获取每日考试")
    public ResponseBean<StartExamDto> everyDayExam(@RequestBody ExaminationRecordParam examRecord) {
        return new ResponseBean<>(answerService.getEveryDayExam(examRecord));
    }

    /**
     * 更新
     * @param examinationDto examinationDto
     * @return ResponseBean
     * @author tangyi
     * @date 2018/11/10 21:15
     */
    @PutMapping
    @PreAuthorize("hasAuthority('exam:exam:edit') or hasAnyRole('" + SecurityConstant.ROLE_ADMIN + "')")
    @ApiOperation(value = "更新考试信息", notes = "根据考试id更新考试的基本信息")
    @ApiImplicitParam(name = "examinationDto", value = "考试实体answer", required = true, dataType = "ExaminationDto")
    @Log("更新考试")
    public ResponseBean<Boolean> updateExamination(@RequestBody @Valid ExaminationDto examinationDto) {
        Examination examination = new Examination();
        BeanUtils.copyProperties(examinationDto, examination);
        if (examinationDto.getCourse() != null){
            examination.setCourseId(examinationDto.getCourse().getId());
        }
        examination.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
        return new ResponseBean<>(examinationService.update(examination) > 0);
    }

    /**
     * 删除考试
     * @param id id
     * @return ResponseBean
     * @author tangyi
     * @date 2018/11/10 21:20
     */
    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('exam:exam:del') or hasAnyRole('" + SecurityConstant.ROLE_ADMIN + "')")
    @ApiOperation(value = "删除考试", notes = "根据ID删除考试")
    @ApiImplicitParam(name = "id", value = "考试ID", required = true, paramType = "path")
    @Log("删除考试")
    public ResponseBean<Boolean> deleteExamination(@PathVariable String id) {
        boolean success = false;
        try {
            Examination examination = new Examination();
            examination.setId(id);
            examination = examinationService.get(examination);
            if (examination != null) {
                examination.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
                success = examinationService.delete(examination) > 0;
            }
        } catch (Exception e) {
            log.error("删除考试失败！", e);
        }
        return new ResponseBean<>(success);
    }

    /**
     * 批量删除
     * @param examinationDto examinationDto
     * @return ResponseBean
     * @author tangyi
     * @date 2018/12/03 22:03
     */
    @PostMapping("deleteAll")
    @PreAuthorize("hasAuthority('exam:exam:del') or hasAnyRole('" + SecurityConstant.ROLE_ADMIN + "')")
    @ApiOperation(value = "批量删除考试", notes = "根据考试id批量删除考试")
    @ApiImplicitParam(name = "examinationDto", value = "考试信息", dataType = "ExaminationDto")
    @Log("批量删除考试")
    public ResponseBean<Boolean> deleteAllExaminations(@RequestBody ExaminationDto examinationDto) {
        boolean success = false;
        try {
            if (StringUtils.isNotEmpty(examinationDto.getIdString())){
                success = examinationService.deleteAll(examinationDto.getIdString().split(",")) > 0;
            }
        } catch (Exception e) {
            log.error("删除考试失败！", e);
        }
        return new ResponseBean<>(success);
    }

    /**
     * 查询考试数量
     * @param tenantCode 租户标识
     * @return ResponseBean
     * @author tangyi
     * @date 2019/3/1 15:30
     */
    @GetMapping("examinationCount")
    public ResponseBean<Integer> findExaminationCount(@RequestParam @NotBlank String tenantCode) {
        Examination examination = new Examination();
        examination.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), tenantCode);
        return new ResponseBean<>(examinationService.findExaminationCount(examination));
    }

    /**
     * 根据考试ID查询题目数量
     * @param examinationId examinationId
     * @return ResponseBean
     * @author tangyi
     * @date 2019/06/18 14:31
     */
    @ApiImplicitParam(name = "examinationId", value = "考试ID", required = true, paramType = "path")
    @GetMapping("/{examinationId}/subjectCount")
    public ResponseBean<Integer> findExaminationSubjectCount(@PathVariable String examinationId) {
        Examination examination = new Examination();
        examination.setId(examinationId);
        return new ResponseBean<>(examinationService.findSubjectCount(examination));
    }

    /**
     * 根据考试ID查询题目数量
     * @param examinationId examinationId /api/exam/v1/examination/getTotalSubjectCount?examinationId=
     * @return ResponseBean
     * @author tangyi
     * @date 2019/06/18 14:31
     */
    @ApiImplicitParam(name = "examinationId", value = "考试ID", required = true, paramType = "path")
    @GetMapping("/getTotalSubjectCount")
    public Result findExamSubjectCount(@RequestParam String examinationId) {
        if(StringUtils.isBlank(examinationId)){
            return Result.error("考试id不能为空！");
        }
        Examination examination = new Examination();
        examination.setId(examinationId);
        return Result.ok().put(examinationService.findSubjectCount(examination));
    }

    /**
     * 根据考试类型查询题目数量 /api/exam/v1/examination/subjectTotalCount?examType=
     * @param examType
     * @return ResponseBean
     * @author tangyi
     * @date 2019/06/18 14:31
     */
    @ApiImplicitParam(name = "examinationId", value = "考试ID", required = true, paramType = "path")
    @GetMapping("/subjectTotalCount")
    public Result findSubjectTotalCount(@RequestParam String examType) {

        if(StringUtils.isBlank(examType)){
            return Result.error("考试类型不能为空！");
        }
        Examination examination = new Examination();

        Long time = System.currentTimeMillis();//获取当前时间精确到毫秒级的时间戳，例：1525849325942
        examination.setExamNumber(examType+"-"+SysUtil.timeStamp2Date(time));

        // 查找活动考试信息
        examination = examinationService.findExaminationByExamNumber(examination);

        if(examination == null){
            return Result.error("考试不存在！");
        }
        Examination examination2 = new Examination();
        examination2.setId(examination.getId());
        return Result.ok().put(examinationService.findSubjectCount(examination2));
    }

    /**
     * 获取历史/api/exam/v1/examination/history/{633091605485719552}/today
     * @param
     * @return ResponseBean
     * @author tangyi
     * @date 2019/10/15 20:44
     */
    @GetMapping("history/{examRecordId}/today")
    @ApiOperation(value = "获取历史", notes = "获取历史")
    @Log("历史")
    public Result today(@PathVariable String examRecordId) {
        log.info(examRecordId);
        return Result.ok("获取成功！").put(examRecordId);
    }

    /**
     * 获取 /api/exam/v1/examination/today
     * @param
     * @return ResponseBean
     * @author tangyi
     * @date 2019/10/15 20:44
     */
    @PostMapping("today")
    @ApiOperation(value = "获取历史", notes = "获取历史")
    @Log("历史")
    public Result todayPost(@RequestBody AnswerDto answerDto) {
        log.info(answerDto.getExamRecordId());
        return Result.ok("获取成功！").put(answerDto.getExamRecordId());
    }


    /**
     * 获取今日考试历史
     * @param
     * @return ResponseBean
     * @author tangyi
     * @date 2018/12/24 20:44
     */
    @GetMapping("todayHistory")
    @ApiOperation(value = "获取今日考试历史", notes = "获取今日考试历史")
    @Log("今日考试历史")
    public Result todayHistory(@RequestParam String examRecordId) {
        //public Result todayHistory(@RequestBody SubjectHistoryDto subjectHistoryDto) {
        //String examRecordId = subjectHistoryDto.getExamRecordId();
        log.info(examRecordId);
        Object examRecordInCache = null;
        examRecordInCache = redisTemplate.opsForValue().get(EVERYDAY_EXAM_RECORD_CACHE_PREFIX + examRecordId);
        Long diffTime = getTimeDiff()/(1000*60);

        if (examRecordInCache != null) {
            log.info("答题历史命中缓存:{}", examRecordInCache);
            log.info(EVERYDAY_EXAM_RECORD_CACHE_PREFIX + examRecordId+"缓存剩余时间：{}",diffTime);
            JSONObject obj = com.alibaba.fastjson.JSON.parseObject((String)examRecordInCache);
            return Result.ok().put( obj);
        }
        PageInfo<AnswerDto> answerDtoPageInfo = new PageInfo<>();
        List<AnswerDto> answerDtos = new ArrayList<>();
        Answer findAnswer = new Answer();
        findAnswer.setExamRecordId(examRecordId);
        // 获取所有的试题答案
        findAnswer.setAnswerType(null);
        PageInfo<Answer> answerPageInfo = answerService.findPage(PageUtil.pageInfo("1", "100", "subject_id", " asc"), findAnswer);

        if (CollectionUtils.isNotEmpty(answerPageInfo.getList())) {
            answerDtos = answerPageInfo.getList().stream().map(tempAnswer -> {
                AnswerDto findAnswerDto = new AnswerDto();
                BeanUtils.copyProperties(tempAnswer, findAnswerDto);
                SubjectDto findSubjectDto = subjectService.get(tempAnswer.getSubjectId(), tempAnswer.getType());
                findAnswerDto.setSubject(findSubjectDto);
                return findAnswerDto;
            }).collect(Collectors.toList());

            answerDtoPageInfo.setList(answerDtos);
            answerDtoPageInfo.setTotal(answerPageInfo.getTotal());
            answerDtoPageInfo.setPageNum(answerPageInfo.getPageNum());
            answerDtoPageInfo.setPageSize(answerPageInfo.getPageSize());
            ObjectNode json = objectMapper.convertValue(answerDtoPageInfo, ObjectNode.class);
            Long diffMint = getTimeDiff()/(1000*60);
            log.info("距离当晚23:59的时差分钟数：{}",diffMint);
            redisTemplate.opsForValue().set(EVERYDAY_EXAM_RECORD_CACHE_PREFIX + examRecordId, json.toString(), diffMint, TimeUnit.MINUTES);

            log.info("今日考试历史获取成功！");
            return Result.ok("今日考试历史获取成功！").put(json);
        }else{
            log.error("今日考试历史获取失败！");
            return Result.error("今日考试历史获取失败！");
        }
    }
    public static long getTimeDiff() {
        long current=System.currentTimeMillis();//当前时间毫秒数
        long zero=current/(1000*3600*24)*(1000*3600*24) - TimeZone.getDefault().getRawOffset();//今天零点零分零秒的毫秒数
        long todayEighteen=zero+24*60*60*1000-1;//今天23点59分59秒的毫秒数
        long tomorrowEighteen =todayEighteen + 24*60*60*1000-1 ; //明天23点59分59秒的毫秒数
        if(current>todayEighteen){
            Long different = tomorrowEighteen - current;
            return different;
        }else{
            Long different = todayEighteen -current;
            return different;
        }
    }

}
