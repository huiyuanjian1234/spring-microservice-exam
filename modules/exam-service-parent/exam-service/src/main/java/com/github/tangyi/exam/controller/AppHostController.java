package com.github.tangyi.exam.controller;

import com.github.tangyi.common.core.utils.Result;
import com.github.tangyi.exam.api.module.AppHost;
import com.github.tangyi.exam.service.AppHostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: AppHostController
 * @Auther: hyj
 * @Date: 2019/10/18 10:57
 */
@Slf4j
@RequestMapping("/v1/AppHost")
@RestController
public class AppHostController {

    public static String MINI_APP_URL = "http://172.31.28.119:10086"; //小程序生产域名

    @Autowired
    private  AppHostService appHostService;

    @GetMapping("/getHost")
    public Result getHost() {
        AppHost appHost = new AppHost();
        appHost.setStatus("1");
        return Result.ok().put(appHostService.getByStatus(appHost));
    }

    @GetMapping("/getHostById")
    public Result getHostById() {
        AppHost appHost = new AppHost();
        appHost.setId("1");
        return Result.ok().put(appHostService.getById(appHost));
    }

    /**
     * 切换小程序域名 http://172.16.0.93:8000/api/exam/v1/AppHost/getHostUrl
     * @return
     */
    @GetMapping("/getHostUrl")
    public Result getHostUrl() {
        AppHost appHost = new AppHost();
        appHost.setStatus("1");
        AppHost dbAppHost = appHostService.getByStatus(appHost);
        if(dbAppHost != null){
            log.info("旧域名：{}",MINI_APP_URL);
            MINI_APP_URL = dbAppHost.getHost();
            log.info("新域名：{}",MINI_APP_URL);
        }
        return Result.ok("切换小程序域名成功！").put(MINI_APP_URL);
    }

}
