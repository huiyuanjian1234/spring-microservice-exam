package com.github.tangyi.exam.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.github.tangyi.common.core.constant.CommonConstant;
import com.github.tangyi.common.core.constant.MqConstant;
import com.github.tangyi.common.core.exceptions.CommonException;
import com.github.tangyi.common.core.service.CrudService;
import com.github.tangyi.common.core.utils.HttpUtils;
import com.github.tangyi.common.core.utils.PageUtil;
import com.github.tangyi.common.core.utils.SysUtil;
import com.github.tangyi.common.core.utils.okhttp.OkHttpUtil;
import com.github.tangyi.common.security.tenant.TenantContextHolder;
import com.github.tangyi.exam.api.constants.AnswerConstant;
import com.github.tangyi.exam.api.constants.ExamExaminationRecordConstant;
import com.github.tangyi.exam.api.constants.ExamSubjectConstant;
import com.github.tangyi.exam.api.dto.*;
import com.github.tangyi.exam.api.feign.ExaminationServiceClient;
import com.github.tangyi.exam.api.module.Answer;
import com.github.tangyi.exam.api.module.Examination;
import com.github.tangyi.exam.api.module.ExaminationRecord;
import com.github.tangyi.exam.api.module.ExaminationSubject;
import com.github.tangyi.exam.controller.AnswerController;
import com.github.tangyi.exam.controller.AppHostController;
import com.github.tangyi.exam.enums.SubjectTypeEnum;
import com.github.tangyi.exam.fegin.MiniProgramClient;
import com.github.tangyi.exam.mapper.AnswerMapper;
import feign.Feign;
import feign.gson.GsonDecoder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 答题service
 *
 * @author tangyi
 * @date 2018/11/8 21:17
 */
@Slf4j
@AllArgsConstructor
@Service
public class AnswerService extends CrudService<AnswerMapper, Answer> {

    @Autowired
    private AppHostService appHostService;

    //private final MiniProgramClient miniProgramClient;
    private final ExaminationServiceClient examinationServiceClient;

    private final AmqpTemplate amqpTemplate;

    private final SubjectService subjectService;

    private final ExamRecordService examRecordService;

    private final ExaminationService examinationService;

    private final ExaminationSubjectService examinationSubjectService;

    //public static final String MINI_APP_URL = AppHostController.MINI_APP_URL;

    public static String SAVE_RECORD_INTEGRAL_URL = "/freelancer-miniapp/exam-record/saveRecordAndIntegral";
    public static String SAVE_MORE_RECORD_INTEGRAL_URL = "/freelancer-miniapp/exam-record/save-more-answer-exam-record";

    public static String SAVE_RECORD_URL = "http://192.168.200.155:10086/saveExamRecord";

    /**
     * 查找答题
     * @param answer answer
     * @return Answer
     * @author tangyi
     * @date 2019/1/3 14:27
     */
    @Override
    @Cacheable(value = "answer#" + CommonConstant.CACHE_EXPIRE, key = "#answer.id")
    public Answer get(Answer answer) {
        return super.get(answer);
    }

    /**
     * 根据用户ID、考试ID、考试记录ID、题目ID查找答题
     * @param answer answer
     * @return Answer
     * @author tangyi
     * @date 2019/01/21 19:41
     */
    public Answer getAnswer(Answer answer) {
        return this.dao.getAnswer(answer);
    }

    /**
     * 更新答题
     * @param answer answer
     * @return int
     * @author tangyi
     * @date 2019/1/3 14:27
     */
    @Override
    @Transactional
    @CacheEvict(value = "answer", key = "#answer.id")
    public int update(Answer answer) {
        return super.update(answer);
    }

    /**
     * 删除答题
     * @param answer answer
     * @return int
     * @author tangyi
     * @date 2019/1/3 14:27
     */
    @Override
    @Transactional
    @CacheEvict(value = "answer", key = "#answer.id")
    public int delete(Answer answer) {
        return super.delete(answer);
    }

    /**
     * 批量删除答题
     * @param ids ids
     * @return int
     * @author tangyi
     * @date 2019/1/3 14:27
     */
    @Override
    @Transactional
    @CacheEvict(value = "answer", allEntries = true)
    public int deleteAll(String[] ids) {
        return super.deleteAll(ids);
    }

    /**
     * 保存
     * @param answer answer
     * @return int
     * @author tangyi
     * @date 2019/04/30 18:03
     */
    @Transactional
    @Override
    public int save(Answer answer) {
        //answer.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
        return super.save(answer);
    }

    /**
     * 保存答题，返回下一题信息
     * @param answerDto answerDto
     * @return SubjectDto
     * @author tangyi
     * @date 2019/05/01 11:42
     */
    @Transactional
    public SubjectDto saveAndNext(AnswerDto answerDto) {
        Answer answer = new Answer();
        BeanUtils.copyProperties(answerDto, answer);
        Answer tempAnswer = this.getAnswer(answer);
        if (tempAnswer != null) {
            tempAnswer.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
            tempAnswer.setAnswer(answer.getAnswer());
            tempAnswer.setType(answer.getType());
            this.update(tempAnswer);
        } else {
            answer.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
            answer.setMarkStatus(AnswerConstant.TO_BE_MARKED);
            this.insert(answer);
        }
        return this.subjectAnswer(answerDto.getSerialNumber(), answer.getExamRecordId(), answerDto.getUserId());
    }

    /**
     * 提交答卷，自动统计选择题得分
     * @param answer answer
     * @return boolean
     * @author tangyi
     * @date 2018/12/26 14:09
     */
    @Transactional
    public void submit(Answer answer) {
        long start = System.currentTimeMillis();
        String currentUsername = answer.getModifier();

        // 查找已提交的题目
        List<Answer> answerList = findList(answer);
        if (CollectionUtils.isEmpty(answerList)){
            return;
        }

        // 成绩
        ExaminationRecord record = new ExaminationRecord();

        // 分类题目
        Map<String, List<Answer>> distinctAnswer = this.distinctAnswer(answerList);
        // TODO 暂时只自动统计选择题，简答题由老师阅卷批改
        if (distinctAnswer.containsKey(SubjectTypeEnum.CHOICES.name())) {
            this.submitChoicesAnswer(distinctAnswer.get(SubjectTypeEnum.CHOICES.name()), record, answer);
        }

        // 如果全部为选择题，则更新状态为统计完成，否则需要阅卷完成后才更改统计状态
        if (!distinctAnswer.containsKey(SubjectTypeEnum.SHORT_ANSWER.name())){
            record.setSubmitStatus(ExamExaminationRecordConstant.STATUS_CALCULATED);
        }

        // 保存成绩
        record.setCommonValue(currentUsername, SysUtil.getSysCode());
        record.setId(answer.getExamRecordId());
        record.setEndTime(record.getCreateDate());
        examRecordService.update(record);
        log.debug("提交答卷，用户名：{}，考试记录ID：{}，耗时：{}ms", currentUsername,answer.getExamRecordId(), System.currentTimeMillis() - start);
    }


    /**
     * 通过mq异步处理
     * 1. 先发送消息
     * 2. 发送消息成功，更新提交状态，发送失败，返回提交失败
     * 3. 消费消息，计算成绩
     * @param answer answer
     * @return boolean
     * @author tangyi
     * @date 2019/05/03 14:35
     */
    @Transactional
    public boolean submitAsync(Answer answer) {
        long start = System.currentTimeMillis();
        String currentUsername = "", applicationCode = SysUtil.getSysCode(), tenantCode = SysUtil.getTenantCode();
        // 获取用户名
        if (!StringUtils.isBlank(answer.getCreator())){
            currentUsername = answer.getCreator();
        }else{
            currentUsername = SysUtil.getUser();
        }
        answer.setModifier(currentUsername);
        answer.setApplicationCode(applicationCode);
        answer.setTenantCode(tenantCode);
        ExaminationRecord examRecord = new ExaminationRecord();
        examRecord.setCommonValue(currentUsername, applicationCode, tenantCode);
        examRecord.setId(answer.getExamRecordId());
        // 提交时间
        examRecord.setEndTime(examRecord.getCreateDate());
        examRecord.setSubmitStatus(ExamExaminationRecordConstant.STATUS_SUBMITTED);
        // 1. 发送消息
        //amqpTemplate.convertAndSend(MqConstant.SUBMIT_EXAMINATION_QUEUE, answer);

        //同步提交答案
        submitExam(answer);

        // 2. 更新考试状态
        boolean success = examRecordService.update(examRecord) > 0;
        log.debug("提交答卷成功，提交人：{}，考试记录ID：{}，耗时：{}ms", currentUsername, examRecord.getId(), System.currentTimeMillis() - start);
        return success;
    }

    /**
     * 统计考试结果
     * @param answer
     */
    public void submitExam(Answer answer) {
        log.debug("处理考试提交ID：{}, 提交人：{}", answer.getExamRecordId(), answer.getModifier());
        try {
            // 异步提交会丢失tenantCode，需要手动设置
            TenantContextHolder.setTenantCode(answer.getTenantCode());
            ExaminationRecord examRecord = new ExaminationRecord();
            examRecord.setId(answer.getExamRecordId());
            examRecord = examRecordService.get(examRecord);
            if (examRecord == null){
                return;
            }

            if (ExamExaminationRecordConstant.STATUS_NOT_SUBMITTED.equals(examRecord.getSubmitStatus())){
                log.warn("考试记录id：{}未提交", examRecord.getId());
            }
            if (ExamExaminationRecordConstant.STATUS_CALCULATE.equals(examRecord.getSubmitStatus())){
                log.warn("考试记录id：{}正在统计成绩，请勿重复提交", examRecord.getId());
            }

            // 更新状态为正在统计
            examRecord.setSubmitStatus(ExamExaminationRecordConstant.STATUS_CALCULATE);

            // 更新成功
            if (examRecordService.update(examRecord) > 0) {
                log.debug("考试记录id：{} 更新状态为统计成功", examRecord.getId());
                this.submit(answer);
            } else {
                log.warn("考试记录id：{} 更新状态为统计失败", examRecord.getId());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 开始考试
     * @param examRecord examRecord
     * @return StartExamDto
     * @author tangyi
     * @date 2019/04/30 23:06
     */
    @Transactional
    public StartExamDto start(ExaminationRecord examRecord) {
        StartExamDto startExamDto = new StartExamDto();
        String currentUsername = "", applicationCode = SysUtil.getSysCode(), tenantCode = SysUtil.getTenantCode();

        // 获取用户名
        if (!StringUtils.isBlank(examRecord.getCreator())){
            currentUsername = examRecord.getCreator();
        }else{
            currentUsername = SysUtil.getUser();
        }

        // 创建考试记录
        if (StringUtils.isEmpty(examRecord.getExaminationId())){
            throw new CommonException("参数校验失败，考试id为空！");
        }

        if (StringUtils.isEmpty(examRecord.getUserId())) {
            throw new CommonException("参数校验失败，用户id为空！");
        }

        Examination examination = new Examination();
        examination.setId(examRecord.getExaminationId());

        // 查找考试信息
        examination = examinationService.get(examination);
        examRecord.setCommonValue(currentUsername, applicationCode, tenantCode);
        examRecord.setStartTime(examRecord.getCreateDate());

        // 默认未提交状态
        examRecord.setSubmitStatus(ExamExaminationRecordConstant.STATUS_NOT_SUBMITTED);

        // 保存考试记录
        if (examRecordService.insert(examRecord) > 0) {
            startExamDto.setExamination(examination);
            startExamDto.setExamRecord(examRecord);

            // 查找第一题
            ExaminationSubject examinationSubject = new ExaminationSubject();
            examinationSubject.setExaminationId(examRecord.getExaminationId());
            examinationSubject.setSerialNumber(ExamSubjectConstant.DEFAULT_SERIAL_NUMBER);
            List<ExaminationSubject> examinationSubjects = examinationSubjectService.findList(examinationSubject);
            if (CollectionUtils.isEmpty(examinationSubjects)) {
                throw new CommonException("序号为1的题目不存在.");
            }
            examinationSubject = examinationSubjects.get(0);

            // 根据题目ID，类型获取题目的详细信息
            SubjectDto subjectDto = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
            subjectDto.setSerialNumber(1);
            // 绑定到dto
            startExamDto.setSubjectDto(subjectDto);
            // 创建第一题的答题
            Answer answer = new Answer();
            answer.setCommonValue(currentUsername, applicationCode, tenantCode);
            answer.setExamRecordId(examRecord.getId());
            answer.setSubjectId(subjectDto.getId());
            // 默认待批改状态
            answer.setMarkStatus(AnswerConstant.TO_BE_MARKED);
            // 保存答题
            this.save(answer);
            subjectDto.setAnswer(answer);
        }

        log.info("startExamDto isNewRecord:{}",startExamDto.getExamRecord().isNewRecord());
        return startExamDto;
    }

    /**
     * 每日考试
     * @param examRecord examRecord
     * @return StartExamDto
     * @author huiyuanjian
     * @date 2019/09/16 23:06
     */
    @Transactional
    public StartExamDto everyDayExam(ExaminationRecordParam examRecord) {
        String examType = examRecord.getExamType();

        StartExamDto startExamDto = new StartExamDto();
        //ExamSubjectDto examSubjectDto = new ExamSubjectDto();
        SubjectDto subjectDto = new SubjectDto();
        String currentUsername = "", applicationCode = "EXAM", tenantCode = "gitee";
        log.info("currentUsername:{}",currentUsername);

        // 获取用户名
        if (!StringUtils.isBlank(examRecord.getCreator())){
            currentUsername = examRecord.getCreator();
        }

//        if (StringUtils.isEmpty(examRecord.getUserId())) {
//            throw new CommonException("参数校验失败，用户id为空！");
//        }

        Examination examination = new Examination();
        // examination.setId(examRecord.getExaminationId());
        // 查找当天每日一题考试信息
        // examination = examinationService.get(examination);
        Long time = System.currentTimeMillis();//获取当前时间精确到毫秒级的时间戳，例：1525849325942
        examination.setExamNumber(examType+"-"+SysUtil.timeStamp2Date(time));
        examination = examinationService.findExaminationByExamNumber(examination);
        examRecord.setCommonValue(currentUsername, applicationCode, tenantCode);
        examRecord.setStartTime(examRecord.getCreateDate());
        examRecord.setExaminationId(examination.getId());
        // 默认未提交状态
        examRecord.setSubmitStatus(ExamExaminationRecordConstant.STATUS_NOT_SUBMITTED);
        // 保存考试记录
        if ( examRecordService.insert(examRecord) > 0) {
            startExamDto.setExamination(examination);
            //examSubjectDto.setExamination(examination);
            startExamDto.setExamRecord(examRecord);
            // 查找第一题
            ExaminationSubject examinationSubject = new ExaminationSubject();
            examinationSubject.setExaminationId(examination.getId());

            examinationSubject.setSerialNumber(ExamSubjectConstant.DEFAULT_SERIAL_NUMBER);
            List<ExaminationSubject> examinationSubjects = examinationSubjectService.findList(examinationSubject);
            if (CollectionUtils.isEmpty(examinationSubjects)) {
                throw new CommonException("序号为1的题目不存在.");
            }
            examinationSubject = examinationSubjects.get(0);
            // 根据题目ID，类型获取题目的详细信息
            subjectDto = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
            subjectDto.setSerialNumber(1);
            //返回解析
            //subjectDto.setAnalysis("");
            // 绑定到dto
            startExamDto.setSubjectDto(subjectDto);

            // 创建第一题的答题
            Answer answer = new Answer();
            answer.setCommonValue(currentUsername, applicationCode, tenantCode);
            answer.setExamRecordId(examRecord.getId());
            answer.setSubjectId(subjectDto.getId());
            // 默认待批改状态
            answer.setMarkStatus(AnswerConstant.TO_BE_MARKED);
            //设置参考答案
            answer.setAnswer(subjectDto.getAnswer().getAnswer());
            // 保存第1题答题
            //this.save(answer);
            subjectDto.setAnswer(answer);
        }
        log.info("startExamDto isNewRecord:{}",startExamDto.getExamRecord().isNewRecord());
        return startExamDto;
    }

    /**
     * 每日考试
     * @param examRecord examRecord
     * @return StartExamDto
     * @author huiyuanjian
     * @date 2019/09/16 23:06
     */
    @Transactional
    public StartExamDto activeExam(ExaminationRecordParam examRecord) {
        String examType = examRecord.getExamType();
        StartExamDto startExamDto = new StartExamDto();
        //ExamSubjectDto examSubjectDto = new ExamSubjectDto();
        SubjectDto subjectDto = new SubjectDto();
        String currentUsername = "", applicationCode = "EXAM", tenantCode = "gitee";
        log.info("currentUsername:{}",currentUsername);

        // 获取用户名
        if (!StringUtils.isBlank(examRecord.getCreator())){
            currentUsername = examRecord.getCreator();
        }

        Examination examination = new Examination();
        examination.setId(examRecord.getExaminationId());

        // 查找考试信息
        examination = examinationService.get(examination);
        log.info("考试信息：{}",examination.toString());
        examRecord.setCommonValue(currentUsername, applicationCode, tenantCode);
        examRecord.setStartTime(examRecord.getCreateDate());
        examRecord.setExaminationId(examination.getId());
        // 默认未提交状态
        examRecord.setSubmitStatus(ExamExaminationRecordConstant.STATUS_NOT_SUBMITTED);
        // 保存考试记录
        if ( examRecordService.insert(examRecord) > 0) {
            startExamDto.setExamination(examination);
            //examSubjectDto.setExamination(examination);
            startExamDto.setExamRecord(examRecord);
            // 查找第一题
            ExaminationSubject examinationSubject = new ExaminationSubject();
            examinationSubject.setExaminationId(examination.getId());

            examinationSubject.setSerialNumber(ExamSubjectConstant.DEFAULT_SERIAL_NUMBER);
            List<ExaminationSubject> examinationSubjects = examinationSubjectService.findList(examinationSubject);
            if (CollectionUtils.isEmpty(examinationSubjects)) {
                throw new CommonException("序号为1的题目不存在.");
            }
            examinationSubject = examinationSubjects.get(0);
            // 根据题目ID，类型获取题目的详细信息
            subjectDto = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
            subjectDto.setSerialNumber(1);
            subjectDto.setAnalysis("");
            // 绑定到dto
            startExamDto.setSubjectDto(subjectDto);

            // 创建第一题的答题
            Answer answer = new Answer();
            answer.setCommonValue(currentUsername, applicationCode, tenantCode);
            answer.setExamRecordId(examRecord.getId());
            answer.setSubjectId(subjectDto.getId());
            // 默认待批改状态
            answer.setMarkStatus(AnswerConstant.TO_BE_MARKED);
            // TODO不需要在开始考试时保存第一题答案
            //this.save(answer);
            subjectDto.setAnswer(answer);
        }
        log.info("startExamDto isNewRecord:{}",startExamDto.getExamRecord().isNewRecord());
        return startExamDto;
    }

    /**
     * 获取每日考试题（未登录）
     * @param examRecord examRecord
     * @return StartExamDto
     * @author huiyuanjian
     * @date 2019/09/16 23:06
     */
    @Transactional
    public StartExamDto getEveryDayExam(ExaminationRecordParam examRecord) {
        String examType = examRecord.getExamType();
        StartExamDto startExamDto = new StartExamDto();
        //ExamSubjectDto examSubjectDto = new ExamSubjectDto();
        SubjectDto subjectDto = new SubjectDto();
        String currentUsername = "", applicationCode = "EXAM", tenantCode = "gitee";

        // 获取用户名
        if (!StringUtils.isBlank(examRecord.getCreator())){
            currentUsername = examRecord.getCreator();
        }else{
            currentUsername = SysUtil.getUser();
        }

        Examination examination = new Examination();
        // examination.setId(examRecord.getExaminationId());
        // 查找当天每日一题考试信息
        // examination = examinationService.get(examination);
        Long time = System.currentTimeMillis();//获取当前时间精确到毫秒级的时间戳，例：1525849325942
        examination.setExamNumber(examType+"-"+SysUtil.timeStamp2Date(time));
        examination = examinationService.findExaminationByExamNumber(examination);
        examRecord.setCommonValue(currentUsername, applicationCode, tenantCode);
        examRecord.setStartTime(examRecord.getCreateDate());
        examRecord.setExaminationId(examination.getId());
        // 默认未提交状态
        examRecord.setSubmitStatus(ExamExaminationRecordConstant.STATUS_NOT_SUBMITTED);

        startExamDto.setExamination(examination);
        //examSubjectDto.setExamination(examination);
        startExamDto.setExamRecord(examRecord);
        // 查找第一题
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setExaminationId(examination.getId());

        examinationSubject.setSerialNumber(ExamSubjectConstant.DEFAULT_SERIAL_NUMBER);
        List<ExaminationSubject> examinationSubjects = examinationSubjectService.findList(examinationSubject);
        if (CollectionUtils.isEmpty(examinationSubjects)) {
            throw new CommonException("序号为1的题目不存在.");
        }
        examinationSubject = examinationSubjects.get(0);
        // 根据题目ID，类型获取题目的详细信息
        subjectDto = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
        subjectDto.setSerialNumber(1);
        subjectDto.setAnalysis("");
        // 绑定到dto
        startExamDto.setSubjectDto(subjectDto);

        // 创建第一题的答题
        Answer answer = new Answer();
        answer.setCommonValue(currentUsername, applicationCode, tenantCode);
        answer.setExamRecordId(examRecord.getId());
        answer.setSubjectId(subjectDto.getId());
        // 默认待批改状态
        answer.setMarkStatus(AnswerConstant.TO_BE_MARKED);
        // 保存答题
        this.save(answer);
        subjectDto.setAnswer(answer);

        log.info("startExamDto isNewRecord:{}",startExamDto.getExamRecord().isNewRecord());
        return startExamDto;
    }


    /**
     * 获取每日一题
     * @return StartExamDto
     * @author huiyuanjian
     * @date 2019/09/18 14:06
     */
    @Transactional
    public SubjectsDto getEveryDayOneSubject() {
        SubjectDto subjectDto = new SubjectDto();
        SubjectsDto subjectsDto = new SubjectsDto();
        List<SubjectDto> subjectDtoList = new ArrayList<>();
        Examination examination = new Examination();

        // 查找当天每日一题考试信息
        Long time = System.currentTimeMillis();//获取当前时间精确到毫秒级的时间戳，例：1525849325942
        examination.setExamNumber("one-"+SysUtil.timeStamp2Date(time));
        examination = examinationService.findExaminationByExamNumber(examination);

        // 查找第一题
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setExaminationId(examination.getId());
        examinationSubject.setSerialNumber(ExamSubjectConstant.DEFAULT_SERIAL_NUMBER);
        List<ExaminationSubject> examinationSubjects = examinationSubjectService.findList(examinationSubject);
        if (CollectionUtils.isEmpty(examinationSubjects)) {
            throw new CommonException("序号为1的题目不存在.");
        }
        examinationSubject = examinationSubjects.get(0);
        // 根据题目ID，类型获取题目的详细信息
        subjectDto = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
        subjectDtoList.add(subjectDto);
        subjectsDto.setSubjectDto(subjectDtoList);
        return subjectsDto;
    }

    /**
     * 获取每日十题连答
     * @return StartExamDto
     * @author huiyuanjian
     * @date 2019/09/18 14:06
     */
    @Transactional
    public SubjectsDto getEveryDayTenSubjects() {
        SubjectDto subjectDto = new SubjectDto();
        SubjectsDto subjectsDto = new SubjectsDto();
        List<SubjectDto> subjectDtoList = new ArrayList<>();

        Examination examination = new Examination();
        // 查找当天10题连答考试信息
        Long time = System.currentTimeMillis();//获取当前时间精确到毫秒级的时间戳，例：1525849325942
        examination.setExamNumber("ten-"+SysUtil.timeStamp2Date(time));
        examination = examinationService.findExaminationByExamNumber(examination);

        // 通过考试id查找考试题目详细信息
        ExaminationSubject examinationSubject = new ExaminationSubject();
        log.info("考试id:{}",examination.getId());
        examinationSubject.setExaminationId(examination.getId());

        List<ExaminationSubject> examinationSubjects = examinationSubjectService.findList(examinationSubject);
        if (CollectionUtils.isEmpty(examinationSubjects)) {
            throw new CommonException("未找到今日十题连答考试题目.");
        }

        for (int i=0; i < examinationSubjects.size();i++){
            examinationSubject = examinationSubjects.get(i);
            // 根据题目ID，类型获取题目的详细信息
            subjectDto = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
            subjectDtoList.add(subjectDto);
        }

        subjectsDto.setSubjectDto(subjectDtoList);
        return subjectsDto;
    }

    /**
     * 根据考试id获取考试题详细信息
     * @return SubjectsDto
     * @author huiyuanjian
     * @date 2019/10/12 14:06
     */
    @Transactional
    public SubjectsDto getExamSubjects(String examinationId) {
        SubjectDto subjectDto = new SubjectDto();
        SubjectsDto subjectsDto = new SubjectsDto();
        List<SubjectDto> subjectDtoList = new ArrayList<>();

        // 通过考试id查找考试题目详细信息
        ExaminationSubject examinationSubject = new ExaminationSubject();
        log.info("考试id:{}",examinationId);
        examinationSubject.setExaminationId(examinationId);

        List<ExaminationSubject> examinationSubjects = examinationSubjectService.findList(examinationSubject);
        if (CollectionUtils.isEmpty(examinationSubjects)) {
            throw new CommonException("未找到今日十题连答考试题目.");
        }

        for (int i=0; i < examinationSubjects.size();i++){
            examinationSubject = examinationSubjects.get(i);
            // 根据题目ID，类型获取题目的详细信息
            subjectDto = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
            subjectDtoList.add(subjectDto);
        }

        subjectsDto.setSubjectDto(subjectDtoList);
        return subjectsDto;
    }

    /**
     * 保存答题，返回下一题信息
     * @param answerDto answerDto
     * @return SubjectDto
     * @author huiyuanjian
     * @date 2019/05/01 11:42
     */
    @Transactional
    public SubjectDto saveAndNextSubject(AnswerDto answerDto) {
        String currentUsername = "", applicationCode = "EXAM", tenantCode = "gitee";

        // 获取用户名
        if (!StringUtils.isBlank(answerDto.getCreator())){
            currentUsername = answerDto.getCreator();
        }else{
            currentUsername = SysUtil.getUser();
        }
        Answer answer = new Answer();
        BeanUtils.copyProperties(answerDto, answer);
        Answer tempAnswer = this.getAnswer(answer);
        if (tempAnswer != null) {
            tempAnswer.setCommonValue(currentUsername, applicationCode, tenantCode);
            tempAnswer.setAnswer(answer.getAnswer());
            tempAnswer.setType(answer.getType());
            tempAnswer.setSubjectTag(answer.getSubjectTag());
            tempAnswer.setUserId(answer.getUserId());
            tempAnswer.setSerialNumber(answer.getSerialNumber());
            log.info("更新答案{}",tempAnswer.toString());
            this.update(tempAnswer);
        } else {
            System.out.println(currentUsername);
            answer.setCommonValue(currentUsername, applicationCode, tenantCode);
            answer.setMarkStatus(AnswerConstant.TO_BE_MARKED);
            log.info("插入新答案{}",answer.toString());
            this.insert(answer);
        }
        return this.getNextSubject(answerDto.getSerialNumber(), answer.getExamRecordId(), answerDto.getUserId());
    }

    /**
     * 获取下一个题
     * getNextChoiceSubject
     * @param serialNumber serialNumber
     * @param examRecordId examRecordId
     * @param userId       userId
     * @return SubjectDto
     * @author huiyuanjian
     * @date 2019/09/19 10:10
     */
    @Transactional
    public SubjectDto getNextSubject(Integer serialNumber, String examRecordId, String userId) {
        ExaminationRecord examRecord = new ExaminationRecord();
        examRecord.setId(examRecordId);
        // 查找考试记录
        examRecord = examRecordService.get(examRecord);
        if (examRecord == null){
            throw new CommonException("考试记录不存在.");
        }
        // 考试ID，题目序号查找关联关系
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setExaminationId(examRecord.getExaminationId());
        examinationSubject.setSerialNumber(serialNumber);
        PageInfo<ExaminationSubject> examinationSubjectPageInfo = examinationSubjectService.findPage(PageUtil.pageInfo(CommonConstant.PAGE_NUM_DEFAULT, CommonConstant.PAGE_SIZE_DEFAULT, "id", CommonConstant.PAGE_ORDER_DEFAULT), examinationSubject);
        if (CollectionUtils.isEmpty(examinationSubjectPageInfo.getList())){
            throw new CommonException("序号为" + serialNumber + "的题目不存在.");
        }
        examinationSubject = examinationSubjectPageInfo.getList().get(0);//取第一条数据
        SubjectDto subject = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
        if (subject == null){
            throw new CommonException("序号为" + serialNumber + "的题目选项不存在.");
        }
        subject.setSerialNumber(serialNumber); // 重新设置题号
        // 查找答题 (直接返回正确答案给小程序服务)
        Answer answer = new Answer();
        answer.setSubjectId(subject.getId());
        answer.setExamRecordId(examRecordId);
        Answer userAnswer = this.getAnswer(answer);
        userAnswer = userAnswer == null ? new Answer() : userAnswer;
        // 设置答题
        subject.setAnswer(userAnswer);
        subject.setExaminationRecordId(examRecordId);
        return subject;
    }

    /**
     * 获取下一个选择题
     * getNextChoiceSubject
     * @param serialNumber serialNumber
     * @param examRecordId examRecordId
     * @param userId       userId
     * @return SubjectDto
     * @author huiyuanjian
     * @date 2019/09/19 10:10
     */
    @Transactional
    public SubjectDto getNextChoiceSubject(Integer serialNumber, String examRecordId, String userId) {
        ExaminationRecord examRecord = new ExaminationRecord();
        examRecord.setId(examRecordId);
        // 查找考试记录
        examRecord = examRecordService.get(examRecord);
        if (examRecord == null){
            throw new CommonException("考试记录不存在.");
        }
        // 考试ID，题目序号查找关联关系
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setExaminationId(examRecord.getExaminationId());
        examinationSubject.setSerialNumber(serialNumber);
        PageInfo<ExaminationSubject> examinationSubjectPageInfo = examinationSubjectService.findPage(PageUtil.pageInfo(CommonConstant.PAGE_NUM_DEFAULT, CommonConstant.PAGE_SIZE_DEFAULT, "id", CommonConstant.PAGE_ORDER_DEFAULT), examinationSubject);
        if (CollectionUtils.isEmpty(examinationSubjectPageInfo.getList())){
            throw new CommonException("序号为" + serialNumber + "的题目不存在.");
        }
        examinationSubject = examinationSubjectPageInfo.getList().get(0);//取第一条数据
        SubjectDto subject = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
        if (subject == null){
            throw new CommonException("序号为" + serialNumber + "的题目选项不存在.");
        }
        subject.setSerialNumber(serialNumber); // 重新设置题号
        // 查找答题 (直接返回正确答案给小程序服务)

        subject.setExaminationRecordId(examRecordId);
        return subject;
    }

    /**
     * 查询题目和答题
     * @param serialNumber serialNumber
     * @param examRecordId examRecordId
     * @param userId       userId
     * @return SubjectDto
     * @author tangyi
     * @date 2019/04/30 17:10
     */
    @Transactional
    public SubjectDto subjectAnswer(Integer serialNumber, String examRecordId, String userId) {
        ExaminationRecord examRecord = new ExaminationRecord();
        examRecord.setId(examRecordId);
        // 查找考试记录
        examRecord = examRecordService.get(examRecord);
        if (examRecord == null){
            throw new CommonException("考试记录不存在.");
        }
        // 考试ID，题目序号查找关联关系
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setExaminationId(examRecord.getExaminationId());
        examinationSubject.setSerialNumber(serialNumber);
        PageInfo<ExaminationSubject> examinationSubjectPageInfo = examinationSubjectService.findPage(PageUtil.pageInfo(CommonConstant.PAGE_NUM_DEFAULT, CommonConstant.PAGE_SIZE_DEFAULT, "id", CommonConstant.PAGE_ORDER_DEFAULT), examinationSubject);
        if (CollectionUtils.isEmpty(examinationSubjectPageInfo.getList())){
            throw new CommonException("序号为" + serialNumber + "的题目不存在.");
        }
        examinationSubject = examinationSubjectPageInfo.getList().get(0);
        SubjectDto subject = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
        if (subject == null){
            throw new CommonException("序号为" + serialNumber + "的题目选项不存在.");
        }
        subject.setSerialNumber(serialNumber); // 重新设置题号
        // 查找答题
        Answer answer = new Answer();
        answer.setSubjectId(subject.getId());
        answer.setExamRecordId(examRecordId);
        Answer userAnswer = this.getAnswer(answer);
        userAnswer = userAnswer == null ? new Answer() : userAnswer;
        // 设置答题
        subject.setAnswer(userAnswer);
        subject.setExaminationRecordId(examRecordId);
        return subject;
    }

    /**
     * 分类答题
     * @param answers answers
     * @return Map
     * @author tangyi
     * @date 2019/06/18 16:32
     */
    private Map<String, List<Answer>> distinctAnswer(List<Answer> answers) {
        Map<String, List<Answer>> distinctMap = new HashMap<>();

        for (Answer answer : answers) {
            // ToDo多选题暂时归类为选择题
            if(answer.getType() == 3){
                answer.setType(0);
            }
        }

        answers.stream()
                .collect(Collectors.groupingBy(Answer::getType, Collectors.toList()))
                .forEach((type, temp) -> {
                    // 匹配类型
                    SubjectTypeEnum subjectType = SubjectTypeEnum.match(type);
                    if (subjectType != null) {
                        switch (subjectType) {
                            case CHOICES:
                                distinctMap.put(SubjectTypeEnum.CHOICES.name(), temp);
                                break;
                            case SHORT_ANSWER:
                                distinctMap.put(SubjectTypeEnum.SHORT_ANSWER.name(), temp);
                                break;
                        }
                    }
                });
        return distinctMap;
    }

    /**
     * 统计选择题
     * @param choicesAnswer choicesAnswer
     * @param record        record
     * @author tangyi
     * @date 2019/06/18 16:39
     */
    private void submitChoicesAnswer(List<Answer> choicesAnswer, ExaminationRecord record, Answer answer) {
        // 查找题目信息
        SubjectDto subjectDto = new SubjectDto();
        subjectDto.setType(SubjectTypeEnum.CHOICES.getValue());
        subjectDto.setIds(choicesAnswer.stream().map(Answer::getSubjectId).distinct().toArray(String[]::new));

        // 查找正确答案
        List<SubjectDto> subjects = subjectService.findListById(subjectDto);
        // 每日1题分数
        List<Integer> everyDayRightScore = new ArrayList<>();
        // 每日1题分数
        List<Integer> everyDayWrongScore = new ArrayList<>();
        // 保存答题正确的题目分数
        List<Integer> rightScore = new ArrayList<>();
        // 每日1题解析
        List<String> analysis = new ArrayList<>();
        // 每日1题正确答案
        List<String> rightAnswer = new ArrayList<>();

        choicesAnswer.forEach(tempAnswer -> {
            // 题目集合
            subjects.stream()
                    // 题目ID、题目答案匹配
                    .filter(tempSubject -> tempSubject.getId().equals(tempAnswer.getSubjectId()) && tempSubject.getAnswer().getAnswer().equalsIgnoreCase(tempAnswer.getAnswer()))
                    // 记录答题正确的成绩
                    .findFirst().ifPresent(right -> {
                rightScore.add(right.getScore());
                everyDayRightScore.add(10);
                analysis.add(right.getAnalysis());
                rightAnswer.add(right.getAnswer().getAnswer());
                tempAnswer.setAnswerType(AnswerConstant.RIGHT);
                tempAnswer.setScore(right.getScore());
                tempAnswer.setMarkStatus(AnswerConstant.MARKED);
            });
        });
        // 统计错题
        choicesAnswer.forEach(tempAnswer -> {
            // 题目集合
            subjects.stream()
                    // 题目ID、题目答案匹配
                    .filter(tempSubject -> tempSubject.getId().equals(tempAnswer.getSubjectId()) && !tempSubject.getAnswer().getAnswer().equalsIgnoreCase(tempAnswer.getAnswer()))
                    // 错题
                    .findFirst()
                    .ifPresent(tempSubject -> {
                        tempAnswer.setAnswerType(AnswerConstant.WRONG);
                        tempAnswer.setScore(0);
                        everyDayWrongScore.add(5);
                        analysis.add(tempSubject.getAnalysis());
                        rightAnswer.add(tempSubject.getAnswer().getAnswer());
                        tempAnswer.setMarkStatus(AnswerConstant.MARKED);
                    });
        });
        // 记录总分、正确题目数、错误题目数
        record.setScore(rightScore.stream().mapToInt(Integer::valueOf).sum());
        // TODO 需要新增记录积分字段
        record.setCorrectNumber(rightScore.size());
        record.setInCorrectNumber(choicesAnswer.size() - rightScore.size());


        System.out.println(answer.getExamNumber());
        // todo将积分结果推送给小程序服务端
        String examNumber = AnswerController.EXAM_TYPE_ONE+"-"+SysUtil.timeStamp2Date(System.currentTimeMillis());

        String tenExamNumber = AnswerController.EXAM_TYPE_TEN+"-"+SysUtil.timeStamp2Date(System.currentTimeMillis());

        String activeExamNumber = AnswerController.EXAM_TYPE_ACTIVE+"-1024-"+SysUtil.timeStamp2Date(System.currentTimeMillis());

        String examType = "1";

        String userId = answer.getUserId();//用户id
        String submitExamNumber = answer.getExamNumber();//提交的考试序号
        log.info("提交的考试序号：{}",submitExamNumber);
        String recordId = answer.getExamRecordId();// 考试记录id
        String examinationId = answer.getExaminationId();// 考试id
        String formId = answer.getFormId();// 小程序formId
        OkHttpUtil okHttpUtil = new OkHttpUtil();

        // 每日一题积分统计one-日期
        log.info("推送考试序号：{}",submitExamNumber);
        if(submitExamNumber.startsWith("one")){

            Map<String, String> mapData = new HashMap<>();
            mapData.put("userId",answer.getUserId());
            mapData.put("examRecordId",answer.getExamRecordId());
            mapData.put("examinationId",answer.getExaminationId());
            mapData.put("subjectId",answer.getSubjectId());
            mapData.put("subjectTag",answer.getSubjectTag());
            mapData.put("formId",answer.getFormId());
            mapData.put("examType","1");
            mapData.put("answerType","1");
            mapData.put("createTime",SysUtil.timeStamp2DateTime(System.currentTimeMillis()));

            JSONObject jb = new JSONObject();
            jb.put("userId",answer.getUserId());
            jb.put("examRecordId",recordId);
            jb.put("examinationId",examinationId);
            jb.put("subjectId",answer.getSubjectId());
            jb.put("subjectTag",answer.getSubjectTag());
            jb.put("formId",answer.getFormId());
            jb.put("examType",1);   //  1:每日1题  2:十题代练
            jb.put("answerType",1); //  1: 错误 0：正确
            jb.put("createTime",SysUtil.timeStamp2DateTime(System.currentTimeMillis()));
            //mapData.put("integral","5");

            if(everyDayRightScore.size() == 1){
                mapData.put("answerType","0");
                jb.put("answerType",0);
                //mapData.put("integral",everyDayRightScore.stream().mapToInt(Integer::valueOf).sum()+"");
            }
            if(everyDayWrongScore.size() == 1){
                mapData.put("answerType","1");
                jb.put("answerType",1);
                //mapData.put("integral",everyDayWrongScore.stream().mapToInt(Integer::valueOf).sum()+"");
            }

            log.info("--向小程序推送每日一题答题数据开始--");

            // fegin 提交
            /*MiniProgramClient miniProgramClient = Feign.builder()
                    .decoder(new GsonDecoder())
                    .target(MiniProgramClient.class, "http://192.168.200.192/freelancer-miniapp/exam-record/saveRecordAndIntegral");

            miniProgramClient.saveRecordAndIntegral(mapData);*/

            try{

                String url = AppHostController.MINI_APP_URL+SAVE_RECORD_INTEGRAL_URL;
                log.info("url:{}",url);

                //Map<String, Object> resultOb = OkHttpUtil.doPost(url,jb.toJSONString());
                Map<String, Object> resultObj = okHttpUtil.doPostJson(url,jb.toJSONString());

                //String result = HttpUtils.post(url,mapData);
                //log.info(result);

                //Map<String, String> resultObj = HttpUtils.post(url,mapData);
                log.info("向小程序推送数据:{}  ", resultObj);

                if(resultObj != null){
                    log.info("向小程数据", resultObj.get("data"));
                    record.setAppSubmitStatus((Integer) resultObj.get("code"));
                    if((Integer) resultObj.get("code") == 1000){
                        if(resultObj.get("data")!=null && resultObj.get("data")!=""){
                            record.setIntegral((Integer) resultObj.get("data"));//保存积分结果
                        }
                    }else{
                        log.error((String) resultObj.get("message"));
                    }
                }else{
                    log.error("推送失败：{}！",resultObj);

                }
            }catch (Exception e){
                log.error("每日一题答题结果推送异常！");
                log.error(e.getMessage());
            }

            //String examinationNumber = examinationServiceClient.findExaminationCount("gitee").getData().toString();
            //log.info(examinationNumber);

            log.info("--向小程序推送每日一题答题数据结束--");
            log.info("推送的数据：{}",jb.toJSONString());
            //mapData.put("analysis",analysis.get(0));
            //mapData.put("rightAnswer",rightAnswer.get(0));

            log.info(jb.toJSONString());
        }else{
            List<ConvergeAnswerDto> convergeAnswerDtos = new ArrayList<>();
            convergeAnswerDtos = choicesAnswer.stream().map(submitAnswer -> {
                ConvergeAnswerDto convergeAnswerDto = new ConvergeAnswerDto();
                convergeAnswerDto.setUserId(userId);
                convergeAnswerDto.setExaminationId(examinationId);
                convergeAnswerDto.setExamRecordId(recordId);
                convergeAnswerDto.setFormId(formId);
                convergeAnswerDto.setExamNumber(submitExamNumber);
                convergeAnswerDto.setSubjectId(submitAnswer.getSubjectId());
                convergeAnswerDto.setMyAnswer(submitAnswer.getAnswer());
                convergeAnswerDto.setAnswerType(submitAnswer.getAnswerType());
                if(tenExamNumber.equals(submitExamNumber)){
                    convergeAnswerDto.setExamType(2); //10题连答
                }else{
                    convergeAnswerDto.setExamType(3); //活动
                }

                SubjectDto findSubjectDto = subjectService.get(submitAnswer.getSubjectId(), submitAnswer.getType());
                convergeAnswerDto.setRightAnswer(findSubjectDto.getAnswer().getAnswer());
                convergeAnswerDto.setSubjectTag(findSubjectDto.getSubjectTag());
                convergeAnswerDto.setCreateTime(SysUtil.timeStamp2DateTime(System.currentTimeMillis()));

                log.info("{}",convergeAnswerDto.toString());
                log.info("userId:{},examinationId:{},examRecordId:{},formId:{},examNumber:{},subjectId:{},myAnswer:{},setAnswerType:{},rightAnswer:{},subjectTag:{},createTime:{}",
                        convergeAnswerDto.getUserId(),convergeAnswerDto.getExaminationId(),convergeAnswerDto.getExamRecordId(),convergeAnswerDto.getFormId(),convergeAnswerDto.getExamNumber(),convergeAnswerDto.getSubjectId(),convergeAnswerDto.getMyAnswer(),convergeAnswerDto.getAnswerType()
                        ,convergeAnswerDto.getRightAnswer(),convergeAnswerDto.getSubjectTag(),convergeAnswerDto.getCreateTime());
                return convergeAnswerDto;
            }).collect(Collectors.toList());

            log.info(convergeAnswerDtos.toString());

            // 多题
            try{
                // 发送答题结果
                // JSONObject jb = new JSONObject();
                // jb.put("examRecordList",JSONObject.toJSONString(convergeAnswerDtos));
                // String url = SAVE_RECORD_URL;
                String url = AppHostController.MINI_APP_URL+SAVE_MORE_RECORD_INTEGRAL_URL;
                log.info("url:{}",url);
                // Map<String, Object> resultObj = OkHttpUtil.doPost(url,JSONObject.toJSONString(convergeAnswerDtos));

                Map<String, Object> resultObj = okHttpUtil.doPostJson(url,JSONObject.toJSONString(convergeAnswerDtos));

                if(resultObj != null){
                    log.info("向小程序推送答题结果:{}  ", resultObj);
                    log.info("向小程返回结果", resultObj.get("data"));
                    record.setAppSubmitStatus((Integer) resultObj.get("code")); //保存10题连答答题返回状态码
                }else{
                    log.error("推送失败：{}！",resultObj);
                }
            }catch (Exception e){
                log.error("推送结果异常！");
                log.error(e.getMessage());
            }
        }

        log.info(answer.getExamNumber());
        log.info(answer.getExaminationId());
        // 循环更新答题的状态
        choicesAnswer.forEach(this::update);
    }

    /**
     * 答题详情
     * @param recordId  recordId
     * @param answerDto answerDto
     * @return AnswerDto
     * @author tangyi
     * @date 2019/06/18 23:05
     */
    public AnswerDto findBySerialNumber(String recordId, AnswerDto answerDto) {
        // 默认序号为1
        if (answerDto.getSerialNumber() == null) {
            answerDto.setSerialNumber(ExamSubjectConstant.DEFAULT_SERIAL_NUMBER);
        }
        ExaminationRecord record = new ExaminationRecord();
        record.setId(recordId);
        record = examRecordService.get(record);
        // 查询该考试和指定序号的题目的关联信息
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setExaminationId(record.getExaminationId());
        examinationSubject.setSerialNumber(answerDto.getSerialNumber());
        examinationSubject = examinationSubjectService.findByExaminationIdAndSerialNumber(examinationSubject);
        if (examinationSubject == null){
            throw new CommonException("序号为" + answerDto.getSerialNumber() + "的题目不存在.");
        }
        // 查询题目的详细信息
        SubjectDto subjectDto = subjectService.get(examinationSubject.getSubjectId(), examinationSubject.getType());
        answerDto.setSubject(subjectDto);
        // 查询答题
        Answer answer = new Answer();
        answer.setSubjectId(subjectDto.getId());
        answer.setExamRecordId(recordId);
        Answer userAnswer = this.getAnswer(answer);
        BeanUtils.copyProperties(userAnswer, answerDto);
        return answerDto;
    }

    /**
     * 完成批改
     * @param examRecord examRecord
     * @return boolean
     * @author tangyi
     * @date 2019/06/19 14:44
     */
    public boolean completeMarking(ExaminationRecord examRecord) {
        long start = System.currentTimeMillis();
        examRecord = examRecordService.get(examRecord);
        if (examRecord == null) {
            throw new CommonException("考试记录不存在.");
        }
        Answer answer = new Answer();
        answer.setExamRecordId(examRecord.getId());
        List<Answer> answers = this.findList(answer);
        if (CollectionUtils.isNotEmpty(answers)) {
            long correctNumber = answers.stream().filter(tempAnswer -> tempAnswer.getAnswerType().equals(AnswerConstant.RIGHT)).count();
            // 总分
            Integer score = answers.stream().mapToInt(Answer::getScore).sum();
            examRecord.setScore(score);
            examRecord.setSubmitStatus(ExamExaminationRecordConstant.STATUS_CALCULATED);
            examRecord.setCorrectNumber((int) correctNumber);
            examRecord.setInCorrectNumber(answers.size() - examRecord.getCorrectNumber());
            examRecordService.update(examRecord);
            log.debug("批改完成，用户名：{}，考试ID：{}，总分：{}，耗时：{}ms", examRecord.getCreator(), examRecord.getExaminationId(), score, System.currentTimeMillis() - start);
        }
        return true;
    }
}
