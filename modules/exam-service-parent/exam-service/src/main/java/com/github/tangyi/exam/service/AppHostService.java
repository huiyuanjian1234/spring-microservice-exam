package com.github.tangyi.exam.service;

import com.github.tangyi.common.core.constant.CommonConstant;
import com.github.tangyi.common.core.service.BaseService;
import com.github.tangyi.common.core.service.CrudService;
import com.github.tangyi.exam.api.module.Answer;
import com.github.tangyi.exam.api.module.AppHost;
import com.github.tangyi.exam.mapper.AnswerMapper;
import com.github.tangyi.exam.mapper.AppHostMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: AppHostService
 * @Auther: hyj
 * @Date: 2019/10/18 10:42
 */
@Slf4j
@AllArgsConstructor
@Service
public class AppHostService extends CrudService<AppHostMapper, AppHost> {

    /**
     * 根据id查找host
     * @param  appHost
     * @return Answer
     * @author AppHost
     * @date 2019/10/18 14:27
     */

    //@Cacheable(value = "appHost#" + CommonConstant.CACHE_EXPIRE, key = "#appHost.id")
    public AppHost getById(AppHost appHost) {
        return this.dao.findById(appHost);
    }

    /**
     * 根据status查找host
     * @param appHost
     * @return AppHost
     * @author hyj
     * @date 2019/10/18 14:27
     */
    //@Cacheable(value = "appHostByStatus#" + CommonConstant.CACHE_EXPIRE, key = "#appHost.status")
    public AppHost getByStatus(AppHost appHost) {
        return this.dao.findByStatus(appHost);
    }

    /**
     * 保存host
     * @param appHost appHost
     * @return int
     * @author hyj
     * @date 2019/10/18 18:03
     */
    @Transactional
    public int add(AppHost appHost) {
        return this.dao.insert(appHost);
    }

    /**
     * 更新host
     * @param appHost appHost
     * @return int
     * @author hyj
     * @date 2019/10/18 10:27
     */
    @Transactional
    @CacheEvict(value = "appHost", key = "#appHost.id")
    public int update(AppHost appHost) {
        return this.dao.update(appHost);
    }

    /**
     * 删除host
     * @param appHost appHost
     * @return int
     * @author hyj
     * @date 2019/10/18 14:27
     */
    @Transactional
    @CacheEvict(value = "appHost", key = "#appHost.id")
    public int delete(AppHost appHost) {
        return this.dao.delete(appHost);
    }

}
