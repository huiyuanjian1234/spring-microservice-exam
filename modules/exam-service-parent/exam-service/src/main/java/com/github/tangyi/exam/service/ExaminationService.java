package com.github.tangyi.exam.service;

import com.esotericsoftware.minlog.Log;
import com.github.pagehelper.PageInfo;
import com.github.tangyi.common.core.constant.CommonConstant;
import com.github.tangyi.common.core.service.CrudService;
import com.github.tangyi.common.core.utils.PageUtil;
import com.github.tangyi.common.core.utils.SysUtil;
import com.github.tangyi.exam.api.dto.SubjectDto;
import com.github.tangyi.exam.api.module.Examination;
import com.github.tangyi.exam.api.module.ExaminationSubject;
import com.github.tangyi.exam.api.module.SubjectChoices;
import com.github.tangyi.exam.mapper.ExaminationMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 考试service
 *
 * @author tangyi
 * @date 2018/11/8 21:19
 */
@Slf4j
@AllArgsConstructor
@Service
public class ExaminationService extends CrudService<ExaminationMapper, Examination> {

    private final SubjectChoicesService subjectChoicesService;

    private final SubjectService subjectService;

    private final ExaminationSubjectService examinationSubjectService;

    /**
     * 查询考试
     * @param examination examination
     * @return Examination
     * @author tangyi
     * @date 2019/1/3 14:06
     */
    @Override
    @Cacheable(value = "examination#" + CommonConstant.CACHE_EXPIRE, key = "#examination.id")
    public Examination get(Examination examination) {
        return super.get(examination);
    }

    /**
     * 更新考试
     * @param examination examination
     * @return int
     * @author tangyi
     * @date 2019/1/3 14:07
     */
    @Override
    @Transactional
    @CacheEvict(value = "examination", key = "#examination.id")
    public int update(Examination examination) {
        return super.update(examination);
    }

    /**
     * 删除考试
     *
     * @param examination examination
     * @return int
     * @author tangyi
     * @date 2019/1/3 14:07
     */
    @Override
    @Transactional
    @CacheEvict(value = "examination", key = "#examination.id")
    public int delete(Examination examination) {
        return super.delete(examination);
    }

    /**
     * 批量删除
     * @param ids ids
     * @return int
     * @author tangyi
     * @date 2018/12/4 9:51
     */
    @Override
    @Transactional
    @CacheEvict(value = "examination", allEntries = true)
    public int deleteAll(String[] ids) {
        return super.deleteAll(ids);
    }

    /**
     * 查询考试数量
     * @param examination examination
     * @return int
     * @author tangyi
     * @date 2019/3/1 15:32
     */
    public int findExaminationCount(Examination examination) {
        return this.dao.findExaminationCount(examination);
    }

    /**
     * 根据examNumber查询考试信息
     * @param examination examination
     * @return int
     * @author tangyi
     * @date 2019/9/18 10:05
     */
    public Examination findExaminationByExamNumber(Examination examination) {
        return this.dao.findByExamNumber(examination).get(0);
    }

    /**
     * 根据考试ID获取题目分页数据
     * @param subjectDto subjectDto
     * @param pageNum    pageNum
     * @param pageSize   pageSize
     * @param sort       sort
     * @param order      order
     * @return PageInfo
     * @author tangyi
     * @date 2019/06/16 16:00
     */
    public PageInfo<SubjectDto> findSubjectPageById(SubjectDto subjectDto, String pageNum, String pageSize, String sort, String order) {
        // 返回结果
        PageInfo<SubjectDto> subjectDtoPageInfo = new PageInfo<>();
        // 查询考试题目关联表
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setTenantCode(SysUtil.getTenantCode());
        examinationSubject.setExaminationId(subjectDto.getExaminationId());
        PageInfo<ExaminationSubject> examinationSubjects = examinationSubjectService.findPage(PageUtil.pageInfo(pageNum, pageSize, sort, order), examinationSubject);
        // 根据题目ID查询题目信息
        List<SubjectDto> subjectDtoList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(examinationSubjects.getList())) {
            // 按题目类型分组，获取对应的ID集合
            subjectDtoList = subjectService.findSubjectDtoList(examinationSubjects.getList());
        }
        subjectDtoPageInfo.setList(subjectDtoList);
        subjectDtoPageInfo.setTotal(examinationSubjects.getTotal());
        subjectDtoPageInfo.setPageNum(examinationSubjects.getPageNum());
        subjectDtoPageInfo.setPageSize(examinationSubjects.getPageSize());
        return subjectDtoPageInfo;
    }

    /**
     * 查询题目数量
     * @param examination examination
     * @return int
     * @author tangyi
     * @date 2019/06/18 14:34
     */
    public int findSubjectCount(Examination examination) {
        ExaminationSubject examinationSubject = new ExaminationSubject();
        examinationSubject.setExaminationId(examination.getId());
        return examinationSubjectService.findSubjectCount(examinationSubject);
    }

    /**
     * 系统自动创建考试卷
     * @param
     * @return int
     * @author huiyuanjian
     * @date 2019/09/17 18:59
     */
    @Transactional
    public int autoCreateExamination(Examination examination,int type) {
        // 初始化
        List<ExaminationSubject> examinationSubjectList = new ArrayList<>();
        int result = 0;
        SubjectDto subject = new SubjectDto();
        subject.setType(0);//选择题

        // 查出来所有的题目
        List<SubjectDto> subjectDtoList = subjectService.findAllListByLimit(subject,examination.getAutoNumber()-1,type);

        log.debug("autoNumber:"+(examination.getAutoNumber()-1));

        // 保存考试题
        if (CollectionUtils.isNotEmpty(subjectDtoList)) {
            for (int i=0;i < subjectDtoList.size(); i++){
                // 保存与考试的关联关系
                ExaminationSubject examinationSubject = new ExaminationSubject();
                SubjectDto subjectDto = subjectDtoList.get(i);
                examinationSubject.setCommonValue("system", SysUtil.getSysCode(), SysUtil.getTenantCode());
                examinationSubject.setExaminationId(examination.getId());// 试卷id
                examinationSubject.setSubjectId(subjectDto.getId());
                examinationSubject.setType(0);
                examinationSubject.setSerialNumber(i+1);
                examinationSubjectList.add(examinationSubject);
            }
            // 批量保存考试题
            result = examinationSubjectService.insertBatch(examinationSubjectList);
        }

        return result;
    }
}
