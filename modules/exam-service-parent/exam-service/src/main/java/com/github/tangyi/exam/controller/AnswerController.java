package com.github.tangyi.exam.controller;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.pagehelper.PageInfo;
import com.github.tangyi.common.core.constant.CommonConstant;
import com.github.tangyi.common.core.model.ResponseBean;
import com.github.tangyi.common.core.utils.PageUtil;
import com.github.tangyi.common.core.utils.Result;
import com.github.tangyi.common.core.utils.SysUtil;
import com.github.tangyi.common.core.web.BaseController;
import com.github.tangyi.common.log.annotation.Log;
import com.github.tangyi.exam.api.dto.AnswerDto;
import com.github.tangyi.exam.api.dto.SubjectDto;
import com.github.tangyi.exam.api.module.Answer;
import com.github.tangyi.exam.api.module.ExaminationRecord;
import com.github.tangyi.exam.service.AnswerService;
import com.github.tangyi.exam.service.ExamRecordService;
import com.github.tangyi.exam.service.SubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 答题controller
 *
 * @author tangyi
 * @date 2018/11/8 21:24
 */
@Slf4j
@AllArgsConstructor
@Api("答题信息管理")
@RestController
@RequestMapping("/v1/answer")
public class AnswerController extends BaseController {

    /**
     * 每日一题考试：one
     */
    public static final String EXAM_TYPE_ONE = "one";

    public static final String EXAM_TYPE_TEN = "ten";

    public static final String EXAM_TYPE_ACTIVE = "active";

    @Autowired
    private ObjectMapper objectMapper;

    private final AnswerService answerService;

    private final SubjectService subjectService;

    private final ExamRecordService examRecordService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    public static final String EVERYDAY_EXAM_RECORD_CACHE_PREFIX = "EveryDayExamRecord:";

    /**
     * 根据ID获取
     *
     * @param id id
     * @return ResponseBean
     * @author tangyi
     * @date 2018/11/10 21:23
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "获取答题信息", notes = "根据答题id获取答题详细信息")
    @ApiImplicitParam(name = "id", value = "答题ID", required = true, dataType = "String", paramType = "path")
    public ResponseBean<Answer> answer(@PathVariable String id) {
        Answer answer = new Answer();
        answer.setId(id);
        return new ResponseBean<>(answerService.get(answer));
    }

    /**
     * 获取分页数据
     * @param pageNum  pageNum
     * @param pageSize pageSize
     * @param sort     sort
     * @param order    order
     * @param answer   answer
     * @return PageInfo
     * @author tangyi
     * @date 2018/11/10 21:25
     */
    @GetMapping("answerList")
    @ApiOperation(value = "获取答题列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = CommonConstant.PAGE_NUM, value = "分页页码", defaultValue = CommonConstant.PAGE_NUM_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.PAGE_SIZE, value = "分页大小", defaultValue = CommonConstant.PAGE_SIZE_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.SORT, value = "排序字段", defaultValue = CommonConstant.PAGE_SORT_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.ORDER, value = "排序方向", defaultValue = CommonConstant.PAGE_ORDER_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = "answer", value = "答题信息", dataType = "Answer")
    })
    public PageInfo<Answer> answerList(@RequestParam(value = CommonConstant.PAGE_NUM, required = false, defaultValue = CommonConstant.PAGE_NUM_DEFAULT) String pageNum,
                                       @RequestParam(value = CommonConstant.PAGE_SIZE, required = false, defaultValue = CommonConstant.PAGE_SIZE_DEFAULT) String pageSize,
                                       @RequestParam(value = CommonConstant.SORT, required = false, defaultValue = CommonConstant.PAGE_SORT_DEFAULT) String sort,
                                       @RequestParam(value = CommonConstant.ORDER, required = false, defaultValue = CommonConstant.PAGE_ORDER_DEFAULT) String order,
                                       Answer answer) {
        answer.setTenantCode(SysUtil.getTenantCode());
        return answerService.findPage(PageUtil.pageInfo(pageNum, pageSize, sort, order), answer);
    }

    /**
     * 创建
     * @param answer answer
     * @return ResponseBean
     * @author tangyi
     * @date 2018/11/10 21:26
     */
    @PostMapping
    @ApiOperation(value = "创建答题", notes = "创建答题")
    @ApiImplicitParam(name = "answer", value = "答题实体answer", required = true, dataType = "Answer")
    @Log("新增答题")
    public ResponseBean<Boolean> addAnswer(@RequestBody @Valid Answer answer) {
        answer.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
        return new ResponseBean<>(answerService.insert(answer) > 0);
    }

    /**
     * 更新
     * @param answer answer
     * @return ResponseBean
     * @author tangyi
     * @date 2018/11/10 21:27
     */
    @PutMapping
    @ApiOperation(value = "更新答题信息", notes = "根据答题id更新答题的基本信息")
    @ApiImplicitParam(name = "answer", value = "答题实体answer", required = true, dataType = "Answer")
    @Log("修改答题")
    public ResponseBean<Boolean> updateAnswer(@RequestBody @Valid Answer answer) {
        answer.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
        return new ResponseBean<>(answerService.update(answer) > 0);
    }

    /**
     * 删除
     * @param id id
     * @return ResponseBean
     * @author tangyi
     * @date 2018/11/10 21:28
     */
    @DeleteMapping("{id}")
    @ApiOperation(value = "删除答题", notes = "根据ID删除答题")
    @ApiImplicitParam(name = "id", value = "答题ID", required = true, paramType = "path")
    @Log("删除答题")
    public ResponseBean<Boolean> deleteAnswer(@PathVariable String id) {
        boolean success = false;
        try {
            Answer answer = answerService.get(id);
            if (answer != null) {
                answer.setCommonValue(SysUtil.getUser(), SysUtil.getSysCode(), SysUtil.getTenantCode());
                success = answerService.delete(answer) > 0;
            }
        } catch (Exception e) {
            log.error("删除答题失败！", e);
        }
        return new ResponseBean<>(success);
    }

    /**
     * 保存
     * @param answer answer
     * @return ResponseBean
     * @author tangyi
     * @date 2018/12/24 20:06
     */
    @PostMapping("save")
    @ApiOperation(value = "保存答题", notes = "保存答题")
    @ApiImplicitParam(name = "answer", value = "答题信息", dataType = "Answer")
    @Log("保存答题")
    public ResponseBean<Boolean> save(@RequestBody @Valid Answer answer) {
        return new ResponseBean<>(answerService.save(answer) > 0);
    }

    /**
     * 保存答题，返回下一题信息
     * @param answer answer
     * @return ResponseBean
     * @author tangyi
     * @date 2019/04/30 18:06
     */
    @PostMapping("saveAndNext")
    @ApiOperation(value = "保存答题", notes = "保存答题")
    @ApiImplicitParam(name = "answer", value = "答题信息", dataType = "Answer")
    public ResponseBean<SubjectDto> saveAndNext(@RequestBody AnswerDto answer) {
        /*if(StringUtils.isBlank(answer.getAnswer())){
            Map<String,Object> result = new HashMap<>();
            result.put("code","400");
            result.put("msg","答案不能为空");
            return new ResponseBean<>(result);
        }*/
        return new ResponseBean<>(answerService.saveAndNext(answer));
    }

    /**
     * 提交答卷
     * @param  answerDto
     * @return ResponseBean
     * @author tangyi
     * @date 2018/12/24 20:44
     */
    @PostMapping("submit")
    @ApiOperation(value = "提交答卷", notes = "提交答卷")
    @ApiImplicitParam(name = "answer", value = "答卷信息", dataType = "Answer")
    @Log("提交答题")
    public Result submit(@RequestBody AnswerDto answerDto) {

        if(StringUtils.isBlank(answerDto.getUserId())){
            log.error("userId不能为空！");
            return Result.error("userId不能为空！");
        }
        if(StringUtils.isBlank(answerDto.getExamRecordId())){
            log.error("examRecordId不能为空！");
            return Result.error("examRecordId不能为空！");
        }
        if(StringUtils.isBlank(answerDto.getExamNumber())){
            log.error("examNumber不能为空！");
            return Result.error("examNumber不能为空！");
        }
        if(StringUtils.isBlank(answerDto.getExaminationId())){
            log.error("examinationId不能为空！");
            return Result.error("examinationId不能为空！");
        }

        // TODO 提交时 关联userId 和答题记录
        ExaminationRecord examinationRecord = new ExaminationRecord();
        examinationRecord.setUserId(answerDto.getUserId());
        //每日一题
        if(answerDto.getExamNumber().startsWith(EXAM_TYPE_ONE)){
            examinationRecord.setExaminationType(1);
        }
        //10题连答
        if(answerDto.getExamNumber().startsWith(EXAM_TYPE_TEN)){
            examinationRecord.setExaminationType(2);
        }
        //活动
        if(answerDto.getExamNumber().startsWith(EXAM_TYPE_ACTIVE)){
            examinationRecord.setExaminationType(3);
        }
        log.info("examinationType:{}",examinationRecord.getExaminationType());
        examinationRecord.setExaminationId(answerDto.getExaminationId());

        /**
         * 考试类型转换 1:one 2:ten 3:active
         */
        log.info("examType:{}",answerDto.getExamType());
        if(!StringUtils.isBlank(answerDto.getExamType())){
            //每日一题
            if("1".equals(answerDto.getExamType())){
                answerDto.setExamNumber(EXAM_TYPE_ONE+"-"+SysUtil.timeStamp2Date(System.currentTimeMillis()));
                examinationRecord.setExaminationType(1);
            }
            //10题连答
            if("2".equals(answerDto.getExamType())){
                answerDto.setExamNumber(EXAM_TYPE_TEN+"-"+SysUtil.timeStamp2Date(System.currentTimeMillis()));
                examinationRecord.setExaminationType(2);
            }
            //活动答题
            if("3".equals(answerDto.getExamType())){
                answerDto.setExamNumber(EXAM_TYPE_ACTIVE+"-"+SysUtil.timeStamp2Date(System.currentTimeMillis()));
                examinationRecord.setExaminationType(3);
            }
            log.info("examinationType:{}",examinationRecord.getExaminationType());
        }
        if(answerDto.getExamNumber().startsWith(EXAM_TYPE_ONE+"-")){
            if(StringUtils.isBlank(answerDto.getSubjectId())){
                log.error("subjectId不能为空！");
                return Result.error("subjectId不能为空！");
            }
            if(StringUtils.isBlank(answerDto.getFormId())){
                log.warn("formId不能为空！");
                return Result.error("formId不能为空！");
            }
        }else {
            answerDto.setSubjectId("");
        }

        Answer answer = new Answer();
        answer.setExamRecordId(answerDto.getExamRecordId());
        answer.setUserId(answerDto.getUserId());
        answer.setExaminationId(answerDto.getExaminationId());
        answer.setExamNumber(answerDto.getExamNumber());
        answer.setFormId(answerDto.getFormId());
        answer.setSubjectId(answerDto.getSubjectId());
        answer.setSubjectTag(answerDto.getSubjectTag());
        answer.setCreator(answerDto.getCreator()==null?answerDto.getUserId():answerDto.getCreator());
        String examNumber = "one-"+SysUtil.timeStamp2Date(System.currentTimeMillis()); // 每日一题

        log.info("userId:{},examRecordId:{},examNumber:{},examinationId:{}",answer.getUserId(),answer.getExamRecordId(),answerDto.getExamNumber(),answerDto.getExaminationId());
        log.info("！！！！！！！提交答卷！！！！！！！");



        // 提交需要判断该用户是否已经打过此试卷（每个用户每套试卷只能答一次）
        List<ExaminationRecord> oldExamRecordList = examRecordService.findList(examinationRecord);
        if(oldExamRecordList.size() > 0 ){
            ExaminationRecord oldExamRecord = oldExamRecordList.get(0);
            Integer oldScore = oldExamRecord.getScore();
            if(oldScore != null){
                log.error("用户id:{} 已答过该套试卷！",answerDto.getUserId());
                return Result.error("请勿重复提交！");
            }
        }

        examinationRecord.setId(answerDto.getExamRecordId());
        examinationRecord.setFormId(answerDto.getFormId());
        examRecordService.update(examinationRecord);

        // 同步处理提交答案
        boolean success = answerService.submitAsync(answer);

        // TODO 查询今日是否已经答题  通过 userId 和 今日考试id (通过每日一题标签获取)查询考试记录是否存在
        // PageInfo<AnswerDto> answerDtoPageInfo = new PageInfo<>();

        if(success){
            /*
              List<AnswerDto> answerDtos = new ArrayList<>();
              Answer findAnswer = new Answer();
              findAnswer.setExamRecordId(answerDto.getExamRecordId());
                // 获取所有的试题答案
                findAnswer.setAnswerType(null);
                PageInfo<Answer> answerPageInfo = answerService.findPage(PageUtil.pageInfo("1", "100", "subject_id", " asc"), findAnswer);

                if (CollectionUtils.isNotEmpty(answerPageInfo.getList())) {
                    answerDtos = answerPageInfo.getList().stream().map(tempAnswer -> {
                        AnswerDto findAnswerDto = new AnswerDto();
                        BeanUtils.copyProperties(tempAnswer, findAnswerDto);
                        SubjectDto findSubjectDto = subjectService.get(tempAnswer.getSubjectId(), tempAnswer.getType());
                        findAnswerDto.setSubject(findSubjectDto);
                        return findAnswerDto;
                    }).collect(Collectors.toList());
                }
                answerDtoPageInfo.setList(answerDtos);
                answerDtoPageInfo.setTotal(answerPageInfo.getTotal());
                answerDtoPageInfo.setPageNum(answerPageInfo.getPageNum());
                answerDtoPageInfo.setPageSize(answerPageInfo.getPageSize());
            */

            ExaminationRecord newExamRecord = examRecordService.get(examinationRecord);
            Integer score = newExamRecord.getIntegral();// 获取积分结果
            Integer appSubmitStatus = newExamRecord.getAppSubmitStatus();// 获取小程序推送状态

            if(appSubmitStatus == null){
                log.error("{} 推送小程序数据失败！",answerDto.getExamNumber());
                return Result.error("提交失败！");
            }else{
                //每日一题结果
                log.info("考试序号：{}",answerDto.getExamNumber());
                if(answerDto.getExamNumber().startsWith("one")){
                    if(appSubmitStatus == 1000){
                        String integral = "5"; //每日一题默认答错得5积分
                        if(score != null){
                            log.info("score:{}",score.toString());
                            integral = score.toString();
                        }else{
                            log.error("用户id:{} 每日一题考试提交失败！",answerDto.getUserId());
                            return Result.error("提交失败！");
                        }
                        log.info("用户id:{} 今日每日一题考试提交成功！获取{}积分",answerDto.getUserId(),integral);
                        return Result.ok("提交成功！").put(integral);

                    }else if(appSubmitStatus == 2006){
                        log.error("用户id:{} 每日一题今日已经提交过，请勿重复提交！",answerDto.getUserId());
                        return Result.error("您今日已答过，请明日再来！");
                    } else{
                        log.error("用户id:{} 每日一题考试提交失败！",answerDto.getUserId());
                        return Result.error("提交失败！");
                    }
                }else{
                    if(appSubmitStatus == 1000){
                        return Result.ok("提交成功！").put("");
                    }else if(appSubmitStatus == 2006){
                        log.error("用户id:{} 请勿重复提交！",answerDto.getUserId());
                        return Result.error("请勿重复提交！");
                    } else{
                        log.error("用户id:{} 考试提交失败！",answerDto.getUserId());
                        return Result.error("提交失败！");
                    }
                }
            }

        }else {
            log.error("{} 考试提交失败！",answerDto.getExamNumber());
            return Result.error("提交失败！");
        }
    }

    /**
     * 获取今日考试历史
     * @param
     * @return ResponseBean
     * @author tangyi
     * @date 2018/12/24 20:44
     */
    @GetMapping("todayHistory")
    @ApiOperation(value = "获取今日考试历史", notes = "获取今日考试历史")
    @Log("今日考试历史")
    public Result todayHistory(@RequestParam String examRecordId) {
    //public Result todayHistory(@RequestBody SubjectHistoryDto subjectHistoryDto) {
        //String examRecordId = subjectHistoryDto.getExamRecordId();
        log.info(examRecordId);
        Object examRecordInCache = null;
        examRecordInCache = redisTemplate.opsForValue().get(EVERYDAY_EXAM_RECORD_CACHE_PREFIX + examRecordId);
        Long diffTime = getTimeDiff()/(1000*60);

        if (examRecordInCache != null) {
            log.info("答题历史命中缓存:{}", examRecordInCache);
            log.info(EVERYDAY_EXAM_RECORD_CACHE_PREFIX + examRecordId+"缓存剩余时间：{}",diffTime);
            JSONObject obj = com.alibaba.fastjson.JSON.parseObject((String)examRecordInCache);
            return Result.ok().put( obj);
        }
        PageInfo<AnswerDto> answerDtoPageInfo = new PageInfo<>();
        List<AnswerDto> answerDtos = new ArrayList<>();
        Answer findAnswer = new Answer();
        findAnswer.setExamRecordId(examRecordId);
        // 获取所有的试题答案
        findAnswer.setAnswerType(null);
        PageInfo<Answer> answerPageInfo = answerService.findPage(PageUtil.pageInfo("1", "100", "subject_id", " asc"), findAnswer);

        if (CollectionUtils.isNotEmpty(answerPageInfo.getList())) {
            answerDtos = answerPageInfo.getList().stream().map(tempAnswer -> {
                AnswerDto findAnswerDto = new AnswerDto();
                BeanUtils.copyProperties(tempAnswer, findAnswerDto);
                SubjectDto findSubjectDto = subjectService.get(tempAnswer.getSubjectId(), tempAnswer.getType());
                findAnswerDto.setSubject(findSubjectDto);
                return findAnswerDto;
            }).collect(Collectors.toList());

            answerDtoPageInfo.setList(answerDtos);
            answerDtoPageInfo.setTotal(answerPageInfo.getTotal());
            answerDtoPageInfo.setPageNum(answerPageInfo.getPageNum());
            answerDtoPageInfo.setPageSize(answerPageInfo.getPageSize());
            ObjectNode json = objectMapper.convertValue(answerDtoPageInfo, ObjectNode.class);
            Long diffMint = getTimeDiff()/(1000*60);
            log.info("距离当晚23:59的时差分钟数：{}",diffMint);
            redisTemplate.opsForValue().set(EVERYDAY_EXAM_RECORD_CACHE_PREFIX + examRecordId, json.toString(), diffMint, TimeUnit.MINUTES);

            log.info("今日考试历史获取成功！");
            return Result.ok("今日考试历史获取成功！").put(json);
        }else{
            log.error("今日考试历史获取失败！");
            return Result.error("今日考试历史获取失败！");
        }
    }

    public static long getTimeDiff() {
        long current=System.currentTimeMillis();//当前时间毫秒数
        long zero=current/(1000*3600*24)*(1000*3600*24) - TimeZone.getDefault().getRawOffset();//今天零点零分零秒的毫秒数
        long todayEighteen=zero+24*60*60*1000-1;//今天23点59分59秒的毫秒数
        long tomorrowEighteen =todayEighteen + 24*60*60*1000-1 ; //明天23点59分59秒的毫秒数
        if(current>todayEighteen){
            Long different = tomorrowEighteen - current;
            return different;
        }else{
            Long different = todayEighteen -current;
            return different;
        }
    }


    /**
     * 获取历史
     * @param
     * @return ResponseBean
     * @author tangyi
     * @date 2019/10/15 20:44
     */
    @GetMapping("history/{examRecordId}/today")
    @ApiOperation(value = "获取历史", notes = "获取历史")
    @Log("历史")
    public Result today(@PathVariable String examRecordId) {
        log.info(examRecordId);
        return Result.ok("获取成功！").put(examRecordId);
    }

    /**
     * 获取历史
     * @param
     * @return ResponseBean
     * @author tangyi
     * @date 2019/10/15 20:44
     */
    @PostMapping("today")
    @ApiOperation(value = "获取历史", notes = "获取历史")
    @Log("历史")
    public Result todayPost(@RequestBody AnswerDto answerDto) {
        log.info(answerDto.getExamRecordId());
        return Result.ok("获取成功！").put(answerDto.getExamRecordId());
    }


    /**
     * 答题列表，包括题目的详情
     * 支持查询正确、错误类型的题目
     * @param recordId recordId
     * @param answer answer
     * @return PageInfo
     * @author tangyi
     * @date 2019/06/18 19:16
     */
    @GetMapping("record/{recordId}/answerListInfo")
    @ApiOperation(value = "获取答题信息列表", notes = "根据考试记录id获取答题详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = CommonConstant.PAGE_NUM, value = "分页页码", defaultValue = CommonConstant.PAGE_NUM_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.PAGE_SIZE, value = "分页大小", defaultValue = CommonConstant.PAGE_SIZE_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.SORT, value = "排序字段", defaultValue = CommonConstant.PAGE_SORT_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = CommonConstant.ORDER, value = "排序方向", defaultValue = CommonConstant.PAGE_ORDER_DEFAULT, dataType = "String"),
            @ApiImplicitParam(name = "recordId", value = "考试记录ID", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "answer", value = "答题信息", dataType = "Answer")
    })
    public PageInfo<AnswerDto> answerListInfo(
            @RequestParam(value = CommonConstant.PAGE_NUM, required = false, defaultValue = CommonConstant.PAGE_NUM_DEFAULT) String pageNum,
            @RequestParam(value = CommonConstant.PAGE_SIZE, required = false, defaultValue = CommonConstant.PAGE_SIZE_DEFAULT) String pageSize,
            @RequestParam(value = CommonConstant.SORT, required = false, defaultValue = CommonConstant.PAGE_SORT_DEFAULT) String sort,
            @RequestParam(value = CommonConstant.ORDER, required = false, defaultValue = CommonConstant.PAGE_ORDER_DEFAULT) String order,
            @PathVariable String recordId, Answer answer) {
        List<AnswerDto> answerDtos = new ArrayList<>();
        answer.setExamRecordId(recordId);

        System.out.println(recordId);
        //answer.setAnswerType(null);
        System.out.println(answer.getAnswerType());

        PageInfo<Answer> answerPageInfo = answerService.findPage(PageUtil.pageInfo(pageNum, pageSize, sort, order), answer);

        if (CollectionUtils.isNotEmpty(answerPageInfo.getList())) {
            answerDtos = answerPageInfo.getList().stream().map(tempAnswer -> {
                AnswerDto answerDto = new AnswerDto();
                BeanUtils.copyProperties(tempAnswer, answerDto);
                SubjectDto subjectDto = subjectService.get(tempAnswer.getSubjectId(), tempAnswer.getType());
                answerDto.setSubject(subjectDto);
                return answerDto;
            }).collect(Collectors.toList());
        }

        PageInfo<AnswerDto> answerDtoPageInfo = new PageInfo<>();
        answerDtoPageInfo.setList(answerDtos);
        answerDtoPageInfo.setTotal(answerPageInfo.getTotal());
        answerDtoPageInfo.setPageNum(answerPageInfo.getPageNum());
        answerDtoPageInfo.setPageSize(answerPageInfo.getPageSize());

        return answerDtoPageInfo;
    }

    /**
     * 答题详情
     * @param recordId recordId
     * @param answer   answer
     * @return ResponseBean
     * @author tangyi
     * @date 2019/06/18 22:50
     */
    @GetMapping("record/{recordId}/answerInfo")
    @ApiOperation(value = "保存答题", notes = "保存答题")
    @ApiImplicitParam(name = "answer", value = "答题信息", dataType = "Answer")
    public ResponseBean<AnswerDto> findBySerialNumber(@PathVariable String recordId, AnswerDto answer) {
        return new ResponseBean<>(answerService.findBySerialNumber(recordId, answer));
    }
}
