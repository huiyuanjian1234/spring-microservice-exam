package com.github.tangyi.exam.mapper;

import com.github.tangyi.common.core.persistence.BaseMapper;
import com.github.tangyi.common.core.persistence.CrudMapper;
import com.github.tangyi.exam.api.module.Answer;
import com.github.tangyi.exam.api.module.AppHost;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: AppHostMapper
 * @Auther: hyj
 * @Date: 2019/10/18 10:33
 */
@Mapper
public interface AppHostMapper extends CrudMapper<AppHost> {

    /**
     * 根据id获取列表数据
     *
     * @param appHost
     * @return List
     */
    AppHost findById(AppHost appHost);
    /**
     * 根据status获取列表数据
     *
     * @param appHost
     * @return List
     */
    AppHost findByStatus(AppHost appHost);

    /**
     * 获取所有数据
     *
     * @return List
     */
    List<AppHost> findAllList();

    /**
     * 插入单条数据
     *
     *
     * @return int
     */
    int insert(AppHost appHost);

    /**
     * 更新单条数据
     *
     * @return int
     */
    int update(AppHost appHost);

    /**
     * 删除单条数据
     *
     * @param  appHost
     * @return int
     */
    int delete(AppHost appHost);
}
