package com.github.tangyi.exam.fegin;

import com.github.tangyi.common.core.utils.Result;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: MiniProgramClientFallbackFactory
 * @Auther: hyj
 * @Date: 2019/10/10 10:21
 */
@Component
public class MiniProgramClientFallbackFactory implements FallbackFactory<MiniProgramClient> {
    @Override
    public MiniProgramClient create(Throwable throwable) {
        return new MiniProgramClientFallback(throwable);
    }
}

class MiniProgramClientFallback implements MiniProgramClient {

    private final Throwable cause;
    private static final Logger LOGGER = LoggerFactory.getLogger(MiniProgramClientFallback.class);

    MiniProgramClientFallback(Throwable cause) {
        this.cause = cause;
        LOGGER.error("服务降级了",cause);
        LOGGER.error("降级",cause.getStackTrace());
        LOGGER.error("MiniProgramClientFallback: {}", cause.getMessage());
    }

    @Override
    public Result saveRecordAndIntegral(Map<String, String> map) {
        LOGGER.info("saveRecordAndIntegral服务降级了");
        return Result.error("saveRecordAndIntegral服务降级了");
    }
}