package com.github.tangyi.common.core.utils;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: PageObject
 * @Auther: hyj
 * @Date: 2019/10/9 16:01
 */
public class PageObject {
    private int pageNo;
    private int pageSize;
    private long totalPages;
    private Object content;
    public int getPageNo() {
        return pageNo;
    }
    public PageObject setPageNo(int pageNo) {
        this.pageNo = pageNo;
        return this;

    }
    public int getPageSize() {
        return pageSize;
    }
    public PageObject setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }
    public long getTotalPages() {
        return totalPages;
    }
    public PageObject setTotalPages(long totalPages) {
        this.totalPages = totalPages;
        return this;
    }
    public Object getContent() {
        return content;
    }
    public PageObject setContent(Object content) {
        this.content = content;
        return this;
    }
}
