package com.github.tangyi.common.core.utils;

import com.github.pagehelper.PageInfo;

import java.util.HashMap;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: Result
 * @Auther: hyj
 * @Date: 2019/10/9 15:59
 */
public class Result extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public Result() {
        super.put("code", "");
        super.put("data", "");
        super.put("message", "");
    }

    public static Result error(String msg) {
        Result r = new Result();
        r.put("code", ResultCode.RESULT_ERROR);
        r.put("message", msg);
        return r;
    }

    public static Result error(int code, String msg) {
        Result r = new Result();
        r.put("code", code);
        r.put("message", msg);
        return r;
    }

    public static Result ok() {
        Result r = new Result();
        r.put("code", ResultCode.RESULT_OK);
        return r;
    }

    public static Result ok(String msg) {
        Result r = new Result();
        r.put("code", ResultCode.RESULT_OK);
        r.put("message", msg);
        return r;
    }

    public Result put(Object value) {
        super.put("data", value);
        return this;
    }

    @SuppressWarnings("rawtypes")
    public Result putPage (PageInfo page) {
        PageObject obj = new PageObject()
                .setContent(page.getList())
                .setPageNo(page.getPageNum())
                .setPageSize(page.getPageSize())
                .setTotalPages(page.getTotal());
        return put(obj);
    }
}
