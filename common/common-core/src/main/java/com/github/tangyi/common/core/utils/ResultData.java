package com.github.tangyi.common.core.utils;

import com.github.pagehelper.PageInfo;

import java.util.HashMap;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: Result
 * @Auther: hyj
 * @Date: 2019/10/9 15:59
 */
public class ResultData extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public ResultData() {
        super.put("code", "");
        super.put("data", "");
        super.put("message", "");
    }

    public static ResultData error(String msg) {
        ResultData r = new ResultData();
        r.put("code", ResultDataCode.RESULT_ERROR);
        r.put("message", msg);
        return r;
    }

    public static ResultData error(int code, String msg) {
        ResultData r = new ResultData();
        r.put("code", code);
        r.put("message", msg);
        return r;
    }

    public static ResultData ok() {
        ResultData r = new ResultData();
        r.put("code", ResultDataCode.RESULT_OK);
        return r;
    }

    public static ResultData ok(String msg) {
        ResultData r = new ResultData();
        r.put("code", ResultDataCode.RESULT_OK);
        r.put("message", msg);
        return r;
    }

    public ResultData put(Object value) {
        super.put("data", value);
        return this;
    }

    @SuppressWarnings("rawtypes")
    public ResultData putPage (PageInfo page) {
        PageObject obj = new PageObject()
                .setContent(page.getList())
                .setPageNo(page.getPageNum())
                .setPageSize(page.getPageSize())
                .setTotalPages(page.getTotal());
        return put(obj);
    }
}
