package com.github.tangyi.common.core.utils;

/**
 * @version : V1.0
 * @Description: TODO
 * @ClassName: ResultCode
 * @Auther: hyj
 * @Date: 2019/10/9 16:00
 */
public class ResultDataCode {
    // 正常
    public static final int RESULT_OK = 200;
    // 异常
    public static final int RESULT_ERROR = 500;
    // token过期或未登录
    public static final int RESULT_AUTH_ERROR = 300;
}
